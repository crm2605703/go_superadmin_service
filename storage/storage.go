package storage

import (
	"admin_service/genproto/admin_service"
	"context"
)

type StorageI interface {
	CloseDB()
	Administration() AdministrationRepoI
	Branch() BranchRepoI
	Event() EventRepoI
	Group() GroupRepoI
	Journal() JournalRepoI
	Manager() ManagerRepoI
	ScheduleWeek() ScheduleWeekRepoI
	Student() StudentRepoI
	SuperAdmin() SuperAdminRepoI
	SupportTeacher() SupportTeacherRepoI
	Task() TaskRepoI
	Teacher() TeacherRepoI
	Lesson() LessonRepoI
	ToDoTask() ToDoTaskRepoI
	AssignStudent() AssignStudentRepoI
	UserBranch() UserBranchRepoI
	Payment() PaymentRepoI
	Report() ReportRepoI
}
type ReportRepoI interface {
	GetStudent(ctx context.Context, req *admin_service.ReportRequest) (*admin_service.StudentList, error)
	GetTeacher(ctx context.Context, req *admin_service.ReportRequest) (*admin_service.TeacherReportList, error)
	GetAdministrator(ctx context.Context, req *admin_service.ReportRequest) (*admin_service.AdminList, error)
	GetSupportTeacher(ctx context.Context, req *admin_service.ReportRequest) (*admin_service.SupportTeacherList, error)
	TeacherReport(ctx context.Context, req *admin_service.TeacherId) (*admin_service.TeacherArrList, error)
	SupportTeacherReport(ctx context.Context, req *admin_service.SupportTeacherId) (*admin_service.SupportArrList, error)
	StudentReport(ctx context.Context, req *admin_service.StudentId) (*admin_service.StudentArrList, error)
}

type PaymentRepoI interface {
	Create(ctx context.Context, req *admin_service.CreatePayment) (resp *admin_service.PaymentPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *admin_service.PaymentPrimaryKey) (resp *admin_service.Payment, err error)
	GetAll(ctx context.Context, req *admin_service.GetListPaymentRequest) (resp *admin_service.GetListPaymentResponse, err error)
	Update(ctx context.Context, req *admin_service.UpdatePayment) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *admin_service.PaymentPrimaryKey) error
}

type UserBranchRepoI interface {
	Create(ctx context.Context, req *admin_service.CreateUserBranch) (resp *admin_service.UserBranchPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *admin_service.UserBranchPrimaryKey) (resp *admin_service.UserBranch, err error)
	GetAll(ctx context.Context, req *admin_service.GetListUserBranchRequest) (resp *admin_service.GetListUserBranchResponse, err error)
	Update(ctx context.Context, req *admin_service.UpdateUserBranch) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *admin_service.UserBranchPrimaryKey) error
}

type AssignStudentRepoI interface {
	Create(ctx context.Context, req *admin_service.CreateAssignStudent) (resp *admin_service.AssignStudentPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *admin_service.AssignStudentPrimaryKey) (resp *admin_service.AssignStudent, err error)
	GetAll(ctx context.Context, req *admin_service.GetListAssignStudentRequest) (resp *admin_service.GetListAssignStudentResponse, err error)
	Update(ctx context.Context, req *admin_service.UpdateAssignStudent) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *admin_service.AssignStudentPrimaryKey) error
}

type ToDoTaskRepoI interface {
	Create(ctx context.Context, req *admin_service.CreateToDoTask) (resp *admin_service.ToDoTaskPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *admin_service.ToDoTaskPrimaryKey) (resp *admin_service.ToDoTask, err error)
	GetAll(ctx context.Context, req *admin_service.GetListToDoTaskRequest) (resp *admin_service.GetListToDoTaskResponse, err error)
	Update(ctx context.Context, req *admin_service.UpdateToDoTask) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *admin_service.ToDoTaskPrimaryKey) error
}

type LessonRepoI interface {
	Create(ctx context.Context, req *admin_service.CreateLesson) (resp *admin_service.LessonPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *admin_service.LessonPrimaryKey) (resp *admin_service.Lesson, err error)
	GetAll(ctx context.Context, req *admin_service.GetListLessonRequest) (resp *admin_service.GetListLessonResponse, err error)
	Update(ctx context.Context, req *admin_service.UpdateLesson) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *admin_service.LessonPrimaryKey) error
}

type AdministrationRepoI interface {
	Create(ctx context.Context, req *admin_service.CreateAdministration) (resp *admin_service.AdministrationPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *admin_service.AdministrationPrimaryKey) (resp *admin_service.Administration, err error)
	GetAll(ctx context.Context, req *admin_service.GetListAdministrationRequest) (resp *admin_service.GetListAdministrationResponse, err error)
	Update(ctx context.Context, req *admin_service.UpdateAdministration) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *admin_service.AdministrationPrimaryKey) error
}

type BranchRepoI interface {
	Create(ctx context.Context, req *admin_service.CreateBranch) (resp *admin_service.BranchPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *admin_service.BranchPrimaryKey) (resp *admin_service.Branch, err error)
	GetAll(ctx context.Context, req *admin_service.GetListBranchRequest) (resp *admin_service.GetListBranchResponse, err error)
	Update(ctx context.Context, req *admin_service.UpdateBranch) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *admin_service.BranchPrimaryKey) error
}

type EventRepoI interface {
	Create(ctx context.Context, req *admin_service.CreateEvent) (resp *admin_service.EventPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *admin_service.EventPrimaryKey) (resp *admin_service.Event, err error)
	GetAll(ctx context.Context, req *admin_service.GetListEventRequest) (resp *admin_service.GetListEventResponse, err error)
	Update(ctx context.Context, req *admin_service.UpdateEvent) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *admin_service.EventPrimaryKey) error
}

type GroupRepoI interface {
	Create(ctx context.Context, req *admin_service.CreateGroup) (resp *admin_service.GroupPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *admin_service.GroupPrimaryKey) (resp *admin_service.Group, err error)
	GetAll(ctx context.Context, req *admin_service.GetListGroupRequest) (resp *admin_service.GetListGroupResponse, err error)
	Update(ctx context.Context, req *admin_service.UpdateGroup) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *admin_service.GroupPrimaryKey) error
}

type JournalRepoI interface {
	Create(ctx context.Context, req *admin_service.CreateJournal) (resp *admin_service.JournalPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *admin_service.JournalPrimaryKey) (resp *admin_service.Journal, err error)
	GetAll(ctx context.Context, req *admin_service.GetListJournalRequest) (resp *admin_service.GetListJournalResponse, err error)
	Update(ctx context.Context, req *admin_service.UpdateJournal) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *admin_service.JournalPrimaryKey) error
}

type ManagerRepoI interface {
	Create(ctx context.Context, req *admin_service.CreateManager) (resp *admin_service.ManagerPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *admin_service.ManagerPrimaryKey) (resp *admin_service.Manager, err error)
	GetAll(ctx context.Context, req *admin_service.GetListManagerRequest) (resp *admin_service.GetListManagerResponse, err error)
	Update(ctx context.Context, req *admin_service.UpdateManager) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *admin_service.ManagerPrimaryKey) error
}

type ScheduleWeekRepoI interface {
	Create(ctx context.Context, req *admin_service.CreateScheduleWeek) (resp *admin_service.ScheduleWeekPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *admin_service.ScheduleWeekPrimaryKey) (resp *admin_service.ScheduleWeek, err error)
	GetAll(ctx context.Context, req *admin_service.GetListScheduleWeekRequest) (resp *admin_service.GetListScheduleWeekResponse, err error)
	Update(ctx context.Context, req *admin_service.UpdateScheduleWeek) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *admin_service.ScheduleWeekPrimaryKey) error
}

type StudentRepoI interface {
	Create(ctx context.Context, req *admin_service.CreateStudent) (resp *admin_service.StudentPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *admin_service.StudentPrimaryKey) (resp *admin_service.Student, err error)
	GetAll(ctx context.Context, req *admin_service.GetListStudentRequest) (resp *admin_service.GetListStudentResponse, err error)
	Update(ctx context.Context, req *admin_service.UpdateStudent) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *admin_service.StudentPrimaryKey) error
}

type SuperAdminRepoI interface {
	Create(ctx context.Context, req *admin_service.CreateSuperAdmin) (resp *admin_service.SuperAdminPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *admin_service.SuperAdminPrimaryKey) (resp *admin_service.SuperAdmin, err error)
	GetAll(ctx context.Context, req *admin_service.GetListSuperAdminRequest) (resp *admin_service.GetListSuperAdminResponse, err error)
	Update(ctx context.Context, req *admin_service.UpdateSuperAdmin) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *admin_service.SuperAdminPrimaryKey) error
}

type SupportTeacherRepoI interface {
	Create(ctx context.Context, req *admin_service.CreateSupportTeacher) (resp *admin_service.SupportTeacherPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *admin_service.SupportTeacherPrimaryKey) (resp *admin_service.SupportTeacher, err error)
	GetAll(ctx context.Context, req *admin_service.GetListSupportTeacherRequest) (resp *admin_service.GetListSupportTeacherResponse, err error)
	Update(ctx context.Context, req *admin_service.UpdateSupportTeacher) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *admin_service.SupportTeacherPrimaryKey) error
}

type TaskRepoI interface {
	Create(ctx context.Context, req *admin_service.CreateTask) (resp *admin_service.TaskPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *admin_service.TaskPrimaryKey) (resp *admin_service.Task, err error)
	GetAll(ctx context.Context, req *admin_service.GetListTaskRequest) (resp *admin_service.GetListTaskResponse, err error)
	Update(ctx context.Context, req *admin_service.UpdateTask) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *admin_service.TaskPrimaryKey) error
}

type TeacherRepoI interface {
	Create(ctx context.Context, req *admin_service.CreateTeacher) (resp *admin_service.TeacherPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *admin_service.TeacherPrimaryKey) (resp *admin_service.Teacher, err error)
	GetAll(ctx context.Context, req *admin_service.GetListTeacherRequest) (resp *admin_service.GetListTeacherResponse, err error)
	Update(ctx context.Context, req *admin_service.UpdateTeacher) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *admin_service.TeacherPrimaryKey) error
}
