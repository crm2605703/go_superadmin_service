package memory

import (
	"admin_service/genproto/admin_service"
	"admin_service/packages/helper"
	"admin_service/storage"
	"context"
	"database/sql"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type assignStudentRepo struct {
	db *pgxpool.Pool
}

func NewAssignStudentRepo(db *pgxpool.Pool) storage.AssignStudentRepoI {
	return &assignStudentRepo{
		db: db,
	}
}

func (c *assignStudentRepo) Create(ctx context.Context, req *admin_service.CreateAssignStudent) (resp *admin_service.AssignStudentPrimaryKey, err error) {

	var (
		id = uuid.New().String()
	)

	query := `
		INSERT INTO "assign_student" (
			id,
			branch_id,
			student_id,
			event_id,
			updated_at
			) VALUES
			($1, $2, $3, $4, NOW())
		`

	_, err = c.db.Exec(ctx,
		query,
		id,
		req.BranchId,
		req.StudentId,
		req.EventId,
	)

	if err != nil {
		return nil, err
	}

	return &admin_service.AssignStudentPrimaryKey{Id: id}, nil
}

func (c *assignStudentRepo) GetByPKey(ctx context.Context, req *admin_service.AssignStudentPrimaryKey) (resp *admin_service.AssignStudent, err error) {

	query := `
		SELECT
			id,
			branch_id,
			student_id,
			event_id,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "assign_student"
		WHERE id = $1
	`

	var (
		Id        sql.NullString
		BranchId  sql.NullString
		StudentId sql.NullString
		EventId   sql.NullString
		CreatedAt sql.NullString
		UpdatedAt sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&Id,
		&BranchId,
		&StudentId,
		&EventId,
		&CreatedAt,
		&UpdatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &admin_service.AssignStudent{
		Id:        Id.String,
		BranchId:  BranchId.String,
		StudentId: StudentId.String,
		EventId:   EventId.String,
		CreatedAt: CreatedAt.String,
		UpdatedAt: UpdatedAt.String,
	}

	return
}

func (c *assignStudentRepo) GetAll(ctx context.Context, req *admin_service.GetListAssignStudentRequest) (resp *admin_service.GetListAssignStudentResponse, err error) {

	resp = &admin_service.GetListAssignStudentResponse{}

	var (
		query  string
		limit  = " LIMIT ALL"
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			branch_id,
			student_id,
			event_id,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "assign_student"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	if len(req.Search) > 0 {
		query += " WHERE title ILIKE '" + req.Search + "' OR title ILIKE '%" + req.Search + "' OR title ILIKE '" + req.Search + "%'"
	} else {
		query += filter
	}
	query += sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	if err != nil {
		return resp, err
	}
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			Id        sql.NullString
			BranchId  sql.NullString
			StudentId sql.NullString
			EventId   sql.NullString
			CreatedAt sql.NullString
			UpdatedAt sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&Id,
			&BranchId,
			&StudentId,
			&EventId,
			&CreatedAt,
			&UpdatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.AssignStudents = append(resp.AssignStudents, &admin_service.AssignStudent{
			Id:        Id.String,
			BranchId:  BranchId.String,
			StudentId: StudentId.String,
			EventId:   EventId.String,
			CreatedAt: CreatedAt.String,
			UpdatedAt: UpdatedAt.String,
		})
	}

	return
}

func (c *assignStudentRepo) Update(ctx context.Context, req *admin_service.UpdateAssignStudent) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "assign_student"
			SET
				id = :id,
				branch_id = :branch_id,
				student_id = :student_id,
				event_id = :event_id,
				updated_at = NOW()
			WHERE
				id = :id`
	params = map[string]interface{}{
		"id":         req.GetId(),
		"branch_id":  req.GetBranchId(),
		"student_id": req.GetStudentId(),
		"event_id":   req.GetEventId(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *assignStudentRepo) Delete(ctx context.Context, req *admin_service.AssignStudentPrimaryKey) error {

	query := `DELETE FROM "assign_student" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return err
	}

	return nil
}
