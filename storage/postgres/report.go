package memory

import (
	"admin_service/genproto/admin_service"
	"admin_service/storage"
	"context"
	"database/sql"
	"errors"

	"github.com/jackc/pgx/v4/pgxpool"
)

type ReportRepo struct {
	db *pgxpool.Pool
}

func NewReportRepo(db *pgxpool.Pool) storage.ReportRepoI {
	return &ReportRepo{
		db: db,
	}
}

func (r *ReportRepo) GetTeacher(ctx context.Context, req *admin_service.ReportRequest) (*admin_service.TeacherReportList, error) {
	rows, err := r.db.Query(
		ctx,
		`SELECT
			name,
			surname,
			phone,
			salary,
			month_worked,
			(salary * month_worked) AS total_sum,
			ielts_score
		FROM teacher
		WHERE branch_id = $1`,
		req.BranchId,
	)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var reportList admin_service.TeacherReportList
	reportList.TeacherList = make([]*admin_service.TeacherResponse, 0)

	for rows.Next() {
		var report admin_service.TeacherResponse

		err := rows.Scan(
			&report.FirstName,
			&report.LastName,
			&report.Phone,
			&report.Salary,
			&report.MonthWorked,
			&report.TotalSum,
			&report.IeltsScore,
		)
		if err != nil {
			return nil, err
		}

		reportList.TeacherList = append(reportList.TeacherList, &report)
	}

	return &reportList, nil
}

func (r *ReportRepo) GetAdministrator(ctx context.Context, req *admin_service.ReportRequest) (*admin_service.AdminList, error) {
	rows, err := r.db.Query(
		ctx,
		`SELECT
			name,
			surname,
			phone,
			salary,
			month_worked,
			(salary * month_worked) AS total_sum,
			ielts_score
		FROM administration
		WHERE branch_id = $1`,
		req.BranchId,
	)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var reportList = admin_service.AdminList{
		AdminList: []*admin_service.AdministratorResponse{},
	}

	for rows.Next() {
		var report = admin_service.AdministratorResponse{}
		var (
			firstName    sql.NullString
			lastName     sql.NullString
			phone        sql.NullString
			salary       sql.NullFloat64
			monthsWorked sql.NullInt64
			totalSum     sql.NullFloat64
			ieltsScore   sql.NullFloat64
		)

		err := rows.Scan(
			&firstName,
			&lastName,
			&phone,
			&salary,
			&monthsWorked,
			&totalSum,
			&ieltsScore,
		)
		if err != nil {
			return nil, err
		}

		report.FirstName = firstName.String
		report.LastName = lastName.String
		report.Phone = phone.String
		report.Salary = salary.Float64
		report.MonthWorked = monthsWorked.Int64
		report.TotalSum = totalSum.Float64
		report.IeltsScore = ieltsScore.Float64

		reportList.AdminList = append(reportList.AdminList, &report)
	}

	if len(reportList.AdminList) == 0 {
		return nil, errors.New("no data found")
	}
	return &reportList, nil
}

func (r *ReportRepo) GetSupportTeacher(ctx context.Context, req *admin_service.ReportRequest) (*admin_service.SupportTeacherList, error) {
	rows, err := r.db.Query(
		ctx,
		`SELECT
			name,
			surname,
			phone,
			salary,
			month_worked,
			(salary * month_worked) AS total_sum,
			ielts_score
		FROM support_teacher
		WHERE branch_id = $1`,
		req.BranchId,
	)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var reportList admin_service.SupportTeacherList
	reportList.SupportTeacherList = make([]*admin_service.SupportTeacherResponse, 0)

	for rows.Next() {
		var report admin_service.SupportTeacherResponse
		var (
			firstName    sql.NullString
			lastName     sql.NullString
			phone        sql.NullString
			salary       sql.NullFloat64
			monthsWorked sql.NullInt64
			totalSum     sql.NullFloat64
			ieltsScore   sql.NullFloat64
		)

		err := rows.Scan(
			&firstName,
			&lastName,
			&phone,
			&salary,
			&monthsWorked,
			&totalSum,
			&ieltsScore,
		)
		if err != nil {
			return nil, err
		}

		report.FirstName = firstName.String
		report.LastName = lastName.String
		report.Phone = phone.String
		report.Salary = salary.Float64
		report.MonthWorked = monthsWorked.Int64
		report.TotalSum = totalSum.Float64
		report.IeltsScore = ieltsScore.Float64

		reportList.SupportTeacherList = append(reportList.SupportTeacherList, &report)
	}

	return &reportList, nil
}

func (r *ReportRepo) GetStudent(ctx context.Context, req *admin_service.ReportRequest) (*admin_service.StudentList, error) {
	rows, err := r.db.Query(
		ctx, `
		SELECT
          	s.name,
		 	s.surname, 
            s.phone,
            s.paid_sum,
            COUNT(c.id) AS course_count,
            s.paid_sum * COUNT(c.id) AS total_sum
        FROM
            student AS s
        JOIN
            "group" AS g ON s.group_id = g.id
        LEFT JOIN
            "group" AS c ON g.id = c.id
        WHERE
            s.branch_id = $1
        GROUP BY
            s.id, s.name, s.surname, s.phone, s.paid_sum
        `,
		req.BranchId,
	)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var reportList admin_service.StudentList
	reportList.StudentList = make([]*admin_service.StudentResponse, 0)

	for rows.Next() {
		var report admin_service.StudentResponse
		var (
			firstName    sql.NullString
			lastName     sql.NullString
			phone        sql.NullString
			paid_sum     sql.NullFloat64
			course_count sql.NullInt64
			totalSum     sql.NullFloat64
		)

		err := rows.Scan(
			&firstName,
			&lastName,
			&phone,
			&paid_sum,
			&course_count,
			&totalSum,
		)
		if err != nil {
			return nil, err
		}

		report.FirstName = firstName.String
		report.LastName = lastName.String
		report.Phone = phone.String
		report.PaidSum = paid_sum.Float64
		report.CourseCount = course_count.Int64
		report.TotalSum = totalSum.Float64

		reportList.StudentList = append(reportList.StudentList, &report)
	}

	if len(reportList.StudentList) == 0 {
		return nil, errors.New("no data found")
	}

	return &reportList, nil
}

func (r *ReportRepo) TeacherReport(ctx context.Context, req *admin_service.TeacherId) (*admin_service.TeacherArrList, error) {
	query := `
		SELECT
			t.name AS teacher_first_name,
			t.surname AS teacher_last_name,
			s.name AS student_first_name,
			s.surname AS student_last_name,
			g.id,
			ta.label AS task_label,
			ta.deadline AS task_deadline
		FROM 
			teacher AS t
		JOIN "group" AS g ON g.teacher_id = t.id
		JOIN student AS s ON s.group_id = g.id
		JOIN journal AS j ON j.group_id = g.id
		JOIN schedule_week AS sw ON sw.group_id = g.id
		JOIN lesson AS l ON l.journal_id = j.id
		JOIN task AS ta ON ta.lesson_id = l.id
		WHERE t.id = $1
	`

	rows, err := r.db.Query(ctx, query, req.Id)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var reportList admin_service.TeacherArrList
	reportList.TeacherList = make([]*admin_service.TeacherList, 0)

	for rows.Next() {
		var report admin_service.TeacherList
		var (
			studentFirstName sql.NullString
			studentLastName  sql.NullString
			teacherFirstName sql.NullString
			teacherLastName  sql.NullString
			groupID          sql.NullString
			taskLabel        sql.NullString
			taskDeadline     sql.NullString
		)

		err := rows.Scan(
			&teacherFirstName,
			&teacherLastName,
			&studentFirstName,
			&studentLastName,
			&groupID,
			&taskLabel,
			&taskDeadline,
		)
		if err != nil {
			return nil, err
		}

		report.StudentFirstName = studentFirstName.String
		report.StudentLastName = studentLastName.String
		report.TeacherFirstName = teacherFirstName.String
		report.TeacherLastName = teacherLastName.String
		report.GroupId = groupID.String
		report.TaskLabel = taskLabel.String
		report.TaskDeadline = taskDeadline.String
		reportList.TeacherList = append(reportList.TeacherList, &report)
	}

	if len(reportList.TeacherList) == 0 {
		return nil, errors.New("no data found")
	}

	return &reportList, nil
}

func (r *ReportRepo) SupportTeacherReport(ctx context.Context, req *admin_service.SupportTeacherId) (*admin_service.SupportArrList, error) {
	query := `
		SELECT 
			s.name AS student_first_name,
			s.surname AS student_last_name,
			g.id,
			sw.start_time AS schedule_start_time,
			sw.end_time AS schedule_end_time,
			ta.label AS task_label,
			ta.deadline AS task_deadline
		FROM
			support_teacher AS st
		JOIN "group" AS g ON g.support_teacher_id = st.id
		JOIN journal AS j ON j.group_id = g.id
		JOIN student AS s ON s.group_id = g.id
		JOIN schedule_week AS sw ON sw.group_id = g.id
		JOIN lesson AS l ON l.journal_id = j.id
		JOIN task AS ta ON ta.lesson_id = l.id
		WHERE st.id = $1
	`

	rows, err := r.db.Query(ctx, query, req.SupportTeacherId)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var reportList admin_service.SupportArrList
	reportList.SupportTeacherReportList = make([]*admin_service.SupportTeacherReportList, 0)

	for rows.Next() {
		var report admin_service.SupportTeacherReportList
		var (
			student_first_name  sql.NullString
			student_last_name   sql.NullString
			group_id            sql.NullString
			schedule_start_time sql.NullString
			schedule_end_time   sql.NullString
			task_label          sql.NullString
			task_deadline       sql.NullString
		)

		err := rows.Scan(
			&student_first_name,
			&student_last_name,
			&group_id,
			&schedule_start_time,
			&schedule_end_time,
			&task_label,
			&task_deadline,
		)
		if err != nil {
			return nil, err
		}

		report.StudentFirstName = student_first_name.String
		report.StudentLastName = student_last_name.String
		report.GroupId = group_id.String
		report.ScheduleStartTime = schedule_start_time.String
		report.ScheduleEndTime = schedule_end_time.String
		report.TaskLabel = task_label.String
		report.TaskDeadline = task_deadline.String
		reportList.SupportTeacherReportList = append(reportList.SupportTeacherReportList, &report)

	}

	// if len(reportList.SupportTeacherReportList) == 0 {
	// 	return nil, errors.New("no data found")
	// }

	return &reportList, nil
}

func (r *ReportRepo) StudentReport(ctx context.Context, req *admin_service.StudentId) (*admin_service.StudentArrList, error) {
	query := `
		SELECT
			s.name AS student_first_name,
			s.surname AS student_last_name,
			g.id,
			sw.start_time AS schedule_start_time,
			sw.end_time AS schedule_end_time,
			t.label AS task_label,
			t.deadline AS task_deadline,
			tdt.score AS task_score,
			e.topic AS event_topic,
			e.day AS event_day,
			e.start_time AS event_start_time
		FROM
			student AS s
		JOIN assign_student AS ast ON ast.student_id = s.id
		JOIN event AS e ON ast.event_id = e.id
		JOIN "group" AS g ON g.id = s.group_id
		JOIN schedule_week AS sw ON sw.group_id = g.id
		JOIN journal AS j  ON j.group_id = g.id
		JOIN lesson AS l ON l.journal_id = j.id
		JOIN task AS t ON t.lesson_id = l.id
		JOIN todo_task AS tdt ON tdt.student_id = s.id
		WHERE s.id = $1
	`

	rows, err := r.db.Query(ctx, query, req.StudentId)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var reportList admin_service.StudentArrList
	reportList.StudentRepList = make([]*admin_service.StudentReportList, 0)

	for rows.Next() {
		var report admin_service.StudentReportList
		var (
			student_first_name  sql.NullString
			student_last_name   sql.NullString
			group_id            sql.NullString
			schedule_start_time sql.NullString
			schedule_end_time   sql.NullString
			task_label          sql.NullString
			task_deadline       sql.NullString
			task_score          sql.NullFloat64
			event_topic         sql.NullString
			event_day           sql.NullString
			event_start_time    sql.NullString
		)

		err := rows.Scan(
			&student_first_name,
			&student_last_name,
			&group_id,
			&schedule_start_time,
			&schedule_end_time,
			&task_label,
			&task_deadline,
			&task_score,
			&event_topic,
			&event_day,
			&event_start_time,
		)
		if err != nil {
			return nil, err
		}

		report.StudentFirstName = student_first_name.String
		report.StudentLastName = student_last_name.String
		report.GroupId = group_id.String
		report.ScheduleStartTime = schedule_start_time.String
		report.ScheduleEndTime = schedule_end_time.String
		report.TaskLabel = task_label.String
		report.TaskDeadline = task_deadline.String
		report.EventTopic = event_topic.String
		report.EventDay = event_day.String
		report.EventStartTime = event_start_time.String
		reportList.StudentRepList = append(reportList.StudentRepList, &report)
	}

	if len(reportList.StudentRepList) == 0 {
		return nil, errors.New("no data found")
	}

	return &reportList, nil
}
