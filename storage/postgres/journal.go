package memory

import (
	"admin_service/genproto/admin_service"
	"admin_service/packages/helper"
	"admin_service/storage"
	"context"
	"database/sql"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type journalRepo struct {
	db *pgxpool.Pool
}

func NewJournalRepo(db *pgxpool.Pool) storage.JournalRepoI {
	return &journalRepo{
		db: db,
	}
}

func (c *journalRepo) Create(ctx context.Context, req *admin_service.CreateJournal) (resp *admin_service.JournalPrimaryKey, err error) {

	var (
		id = uuid.New().String()
	)

	query := `
		INSERT INTO "journal" (
			id,
			schedules_id,
			group_id,
			"from",
			"to",
			updated_at
			) VALUES
			($1, $2, $3, $4, $5, NOW())
		`

	_, err = c.db.Exec(ctx,
		query,
		id,
		req.SchedulesId,
		req.GroupId,
		req.From,
		req.To,
	)

	if err != nil {
		return nil, err
	}

	return &admin_service.JournalPrimaryKey{Id: id}, nil
}

func (c *journalRepo) GetByPKey(ctx context.Context, req *admin_service.JournalPrimaryKey) (resp *admin_service.Journal, err error) {

	query := `
		SELECT
			id,
			schedules_id,
			group_id,
			"from",
			"to",
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "journal"
		WHERE id = $1
	`

	var (
		Id          sql.NullString
		SchedulesId sql.NullString
		GroupId     sql.NullString
		From        sql.NullString
		To          sql.NullString
		CreatedAt   sql.NullString
		UpdatedAt   sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&Id,
		&SchedulesId,
		&GroupId,
		&From,
		&To,
		&CreatedAt,
		&UpdatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &admin_service.Journal{
		Id:          Id.String,
		SchedulesId: SchedulesId.String,
		GroupId:     GroupId.String,
		From:        From.String,
		To:          To.String,
		CreatedAt:   CreatedAt.String,
		UpdatedAt:   UpdatedAt.String,
	}

	return
}

func (c *journalRepo) GetAll(ctx context.Context, req *admin_service.GetListJournalRequest) (resp *admin_service.GetListJournalResponse, err error) {

	resp = &admin_service.GetListJournalResponse{}

	var (
		query  string
		limit  = " LIMIT ALL"
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			schedules_id,
			group_id,
			"from",
			"to",
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "journal"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	if len(req.Search) > 0 {
		query += " WHERE from ILIKE '" + req.Search + "' OR from ILIKE '%" + req.Search + "' OR from ILIKE '" + req.Search + "%'"
		query += " OR to ILIKE '" + req.Search + "' OR to ILIKE '%" + req.Search + "' OR to ILIKE '" + req.Search + "%'"
	} else {
		query += filter
	}
	query += sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	if err != nil {
		return resp, err
	}
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			Id          sql.NullString
			SchedulesId sql.NullString
			GroupId     sql.NullString
			From        sql.NullString
			To          sql.NullString
			CreatedAt   sql.NullString
			UpdatedAt   sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&Id,
			&SchedulesId,
			&GroupId,
			&From,
			&To,
			&CreatedAt,
			&UpdatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Journals = append(resp.Journals, &admin_service.Journal{
			Id:          Id.String,
			SchedulesId: SchedulesId.String,
			GroupId:     GroupId.String,
			From:        From.String,
			To:          To.String,
			CreatedAt:   CreatedAt.String,
			UpdatedAt:   UpdatedAt.String,
		})
	}

	return
}

func (c *journalRepo) Update(ctx context.Context, req *admin_service.UpdateJournal) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "journal"
			SET
				id = :id,
				schedules_id = :schedules_id,
				group_id = :group_id,
				"from" = :"from",
				"to" = :"to",
				updated_at = NOW()
			WHERE
				id = :id`
	params = map[string]interface{}{
		"id":           req.GetId(),
		"schedules_id": req.GetSchedulesId(),
		"group_id":     req.GetGroupId(),
		"from":         req.GetFrom(),
		"to":           req.GetTo(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *journalRepo) Delete(ctx context.Context, req *admin_service.JournalPrimaryKey) error {

	query := `DELETE FROM "journal" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return err
	}

	return nil
}
