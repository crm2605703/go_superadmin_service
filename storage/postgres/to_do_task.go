package memory

import (
	"admin_service/genproto/admin_service"
	"admin_service/packages/helper"
	"admin_service/storage"
	"context"
	"database/sql"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type toDoTaskRepo struct {
	db *pgxpool.Pool
}

func NewToDoTaskRepo(db *pgxpool.Pool) storage.ToDoTaskRepoI {
	return &toDoTaskRepo{
		db: db,
	}
}

func (c *toDoTaskRepo) Create(ctx context.Context, req *admin_service.CreateToDoTask) (resp *admin_service.ToDoTaskPrimaryKey, err error) {

	var (
		id = uuid.New().String()
	)

	query := `
		INSERT INTO "todo_task" (
			id,
			student_id,
			task_id,
			score,
			updated_at
			) VALUES
			($1, $2, $3, $4, NOW())
		`

	_, err = c.db.Exec(ctx,
		query,
		id,
		req.StudentId,
		req.TaskId,
		req.Score,
	)

	if err != nil {
		return nil, err
	}

	return &admin_service.ToDoTaskPrimaryKey{Id: id}, nil
}

func (c *toDoTaskRepo) GetByPKey(ctx context.Context, req *admin_service.ToDoTaskPrimaryKey) (resp *admin_service.ToDoTask, err error) {

	query := `
		SELECT
			id,
			student_id,
			task_id,
			score,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "todo_task"
		WHERE id = $1
	`

	var (
		Id        sql.NullString
		StudentId sql.NullString
		TaskId    sql.NullString
		Score     sql.NullFloat64
		CreatedAt sql.NullString
		UpdatedAt sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&Id,
		&StudentId,
		&TaskId,
		&Score,
		&CreatedAt,
		&UpdatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &admin_service.ToDoTask{
		Id:        Id.String,
		StudentId: StudentId.String,
		TaskId:    TaskId.String,
		Score:     int64(Score.Float64),
		CreatedAt: CreatedAt.String,
		UpdatedAt: UpdatedAt.String,
	}

	return
}

func (c *toDoTaskRepo) GetAll(ctx context.Context, req *admin_service.GetListToDoTaskRequest) (resp *admin_service.GetListToDoTaskResponse, err error) {

	resp = &admin_service.GetListToDoTaskResponse{}

	var (
		query  string
		limit  = " LIMIT ALL"
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			student_id,
			task_id,
			score,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "todo_task"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	if len(req.Search) > 0 {
		query += " WHERE score ILIKE '" + req.Search + "' OR score ILIKE '%" + req.Search + "' OR score ILIKE '" + req.Search + "%'"
	} else {
		query += filter
	}
	query += sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	if err != nil {
		return resp, err
	}
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			Id        sql.NullString
			StudentId sql.NullString
			TaskId    sql.NullString
			Score     sql.NullFloat64
			CreatedAt sql.NullString
			UpdatedAt sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&Id,
			&StudentId,
			&TaskId,
			&Score,
			&CreatedAt,
			&UpdatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.ToDoTasks = append(resp.ToDoTasks, &admin_service.ToDoTask{
			Id:        Id.String,
			StudentId: StudentId.String,
			TaskId:    TaskId.String,
			Score:     int64(Score.Float64),
			CreatedAt: CreatedAt.String,
			UpdatedAt: UpdatedAt.String,
		})
	}

	return
}

func (c *toDoTaskRepo) Update(ctx context.Context, req *admin_service.UpdateToDoTask) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "todo_task"
			SET
				id = :id,
				student_id = :student_id,
				task_id = :task_id,
				score = :score,
				updated_at = NOW()
			WHERE
				id = :id`
	params = map[string]interface{}{
		"id":         req.GetId(),
		"student_id": req.GetStudentId(),
		"task_id":    req.GetTaskId(),
		"score":      req.GetScore(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *toDoTaskRepo) Delete(ctx context.Context, req *admin_service.ToDoTaskPrimaryKey) error {

	query := `DELETE FROM "todo_task" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return err
	}

	return nil
}
