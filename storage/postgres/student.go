package memory

import (
	"admin_service/genproto/admin_service"
	"admin_service/packages/helper"
	"admin_service/storage"
	"context"
	"database/sql"
	"fmt"
	"strconv"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type studentRepo struct {
	db *pgxpool.Pool
}

func NewStudentRepo(db *pgxpool.Pool) storage.StudentRepoI {
	return &studentRepo{
		db: db,
	}
}

func (c *studentRepo) Create(ctx context.Context, req *admin_service.CreateStudent) (resp *admin_service.StudentPrimaryKey, err error) {

	var (
		id           = uuid.New().String()
		increment_id = ""
		maxQuery     = `SELECT MAX(increment_id) FROM "student"`
		digit        = 0
	)

	err = c.db.QueryRow(ctx, maxQuery).Scan(&increment_id)
	if err != nil {
		if err.Error() != "can't scan into dest[0]: cannot scan null into *string" && err.Error() != "no rows in result set" {
			return resp, err
		} else {
			increment_id = "A0000000"
		}
	}
	if len(increment_id) > 2 {
		digit, err = strconv.Atoi(increment_id[2:])
		if err != nil {
			return resp, err
		}
	}
	fmt.Println("__________BUG_________-	", req)

	query := `
		INSERT INTO "student" (
			id,
			increment_id,
			branch_id,
			group_id,
			login,
			password,
			name,
			surname,
			phone,
			paid_sum,
			course_count,
			journal_id,
			total_sum,
			updated_at
			) VALUES
			($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, NOW())
		`

	_, err = c.db.Exec(ctx,
		query,
		id,
		"S"+helper.GetSerialId(digit),
		req.BranchId,
		req.GroupId,
		req.Login,
		req.Password,
		req.Name,
		req.Surname,
		req.Phone,
		req.PaidSum,
		req.CourseCount,
		req.JournalId,
		req.TotalSum,
	)

	if err != nil {
		return nil, err
	}

	return &admin_service.StudentPrimaryKey{Id: id}, nil
}

func (c *studentRepo) GetByPKey(ctx context.Context, req *admin_service.StudentPrimaryKey) (resp *admin_service.Student, err error) {

	query := `
		SELECT
			id,
			increment_id,
			branch_id,
			group_id,
			login,
			password,
			name,
			surname,
			phone,
			paid_sum,
			course_count,
			journal_id,
			total_sum,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "student"
		WHERE id = $1
	`

	var (
		Id          sql.NullString
		IncrementId sql.NullString
		BranchId    sql.NullString
		GroupId     sql.NullString
		Login       sql.NullString
		Password    sql.NullString
		Name        sql.NullString
		Surname     sql.NullString
		Phone       sql.NullString
		PaidSum     sql.NullFloat64
		CourseCount sql.NullInt64
		JournaId    sql.NullString
		TotalSum    sql.NullFloat64
		CreatedAt   sql.NullString
		UpdatedAt   sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&Id,
		&IncrementId,
		&BranchId,
		&GroupId,
		&Login,
		&Password,
		&Name,
		&Surname,
		&Phone,
		&PaidSum,
		&CourseCount,
		&JournaId,
		&TotalSum,
		&CreatedAt,
		&UpdatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &admin_service.Student{
		Id:          Id.String,
		IncrementId: IncrementId.String,
		BranchId:    BranchId.String,
		GroupId:     GroupId.String,
		Login:       Login.String,
		Password:    Password.String,
		Name:        Name.String,
		Surname:     Surname.String,
		Phone:       Phone.String,
		PaidSum:     float32(PaidSum.Float64),
		CourseCount: CourseCount.Int64,
		JournalId:   JournaId.String,
		TotalSum:    float32(TotalSum.Float64),
		CreatedAt:   CreatedAt.String,
		UpdatedAt:   UpdatedAt.String,
	}

	return
}

func (c *studentRepo) GetAll(ctx context.Context, req *admin_service.GetListStudentRequest) (resp *admin_service.GetListStudentResponse, err error) {

	resp = &admin_service.GetListStudentResponse{}

	var (
		query  string
		limit  = " LIMIT ALL"
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			increment_id,
			branch_id,
			group_id,
			login,
			password,
			name,
			surname,
			phone,
			paid_sum,
			course_count,
			journal_id,
			total_sum,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "student"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	if len(req.Search) > 0 {
		query += " WHERE increment_id ILIKE '" + req.Search + "' OR increment_id ILIKE '%" + req.Search + "' OR increment_id ILIKE '" + req.Search + "%'"
		query += " OR name ILIKE '" + req.Search + "' OR name ILIKE '%" + req.Search + "' OR name ILIKE '" + req.Search + "%'"
		query += " OR surname ILIKE '" + req.Search + "' OR surname ILIKE '%" + req.Search + "' OR surname ILIKE '" + req.Search + "%'"
		query += " OR phone ILIKE '" + req.Search + "' OR phone ILIKE '%" + req.Search + "' OR phone ILIKE '" + req.Search + "%'"
		query += " OR branch_id ILIKE '" + req.Search + "' OR branch_id ILIKE '%" + req.Search + "' OR branch_id ILIKE '" + req.Search + "%'"
	} else {
		query += filter
	}
	query += sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	if err != nil {
		return resp, err
	}
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			Id          sql.NullString
			IncrementId sql.NullString
			BranchId    sql.NullString
			GroupId     sql.NullString
			Login       sql.NullString
			Password    sql.NullString
			Name        sql.NullString
			Surname     sql.NullString
			Phone       sql.NullString
			PaidSum     sql.NullFloat64
			CourseCount sql.NullInt64
			JournaId    sql.NullString
			TotalSum    sql.NullFloat64
			CreatedAt   sql.NullString
			UpdatedAt   sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&Id,
			&IncrementId,
			&BranchId,
			&GroupId,
			&Login,
			&Password,
			&Name,
			&Surname,
			&Phone,
			&PaidSum,
			&CourseCount,
			&JournaId,
			&TotalSum,
			&CreatedAt,
			&UpdatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Students = append(resp.Students, &admin_service.Student{
			Id:          Id.String,
			IncrementId: IncrementId.String,
			BranchId:    BranchId.String,
			GroupId:     GroupId.String,
			Login:       Login.String,
			Password:    Password.String,
			Name:        Name.String,
			Surname:     Surname.String,
			Phone:       Phone.String,
			PaidSum:     float32(PaidSum.Float64),
			CourseCount: CourseCount.Int64,
			JournalId:   JournaId.String,
			TotalSum:    float32(TotalSum.Float64),
			CreatedAt:   CreatedAt.String,
			UpdatedAt:   UpdatedAt.String,
		})
	}

	return
}

func (c *studentRepo) Update(ctx context.Context, req *admin_service.UpdateStudent) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "student"
			SET
				id = :id,
				branch_id = :branch_id,
				group_id = :group_id,
				login = :login,
				password = :password,
				name = :name,
				surname = :surname,
				phone = :phone,
				paid_sum = :paid_sum,
				course_count = :course_count,
				journal_id = :journal_id,
				total_sum = :total_sum,
				updated_at = NOW()
			WHERE
				id = :id`
	params = map[string]interface{}{
		"id":           req.GetId(),
		"branch_id":    req.GetBranchId(),
		"login":        req.GetLogin(),
		"password":     req.GetPassword(),
		"name":         req.GetName(),
		"surname":      req.GetSurname(),
		"phone":        req.GetPhone(),
		"paid_sum":     req.GetPaidSum(),
		"course_count": req.GetCourseCount(),
		"journal_id":   req.GetJournalId(),
		"total_sum":    req.GetTotalSum(),
		"group_id":     req.GetGroupId(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *studentRepo) Delete(ctx context.Context, req *admin_service.StudentPrimaryKey) error {

	query := `DELETE FROM "student" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return err
	}

	return nil
}
