package memory

import (
	"admin_service/genproto/admin_service"
	"admin_service/packages/helper"
	"admin_service/storage"
	"context"
	"database/sql"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type scheduleWeekRepo struct {
	db *pgxpool.Pool
}

func NewScheduleWeekRepo(db *pgxpool.Pool) storage.ScheduleWeekRepoI {
	return &scheduleWeekRepo{
		db: db,
	}
}

func (c *scheduleWeekRepo) Create(ctx context.Context, req *admin_service.CreateScheduleWeek) (resp *admin_service.ScheduleWeekPrimaryKey, err error) {

	var (
		id = uuid.New().String()
	)

	query := `
		INSERT INTO "schedule_week" (
			id,
			group_id,
			group_type,
			number_of_students,
			start_time,
			end_time,
			branch_id,
			teacher_id,
			support_teacher_id,
			updated_at
			) VALUES
			($1, $2, $3, $4, $5, $6, $7, $8, $9, NOW())
		`

	_, err = c.db.Exec(ctx,
		query,
		id,
		req.GroupId,
		req.GroupType,
		req.NumberOfStudents,
		req.StartTime,
		req.EndTime,
		req.BranchId,
		req.TeacherId,
		req.SupportTeacherId,
	)

	if err != nil {
		return nil, err
	}

	return &admin_service.ScheduleWeekPrimaryKey{Id: id}, nil
}

func (c *scheduleWeekRepo) GetByPKey(ctx context.Context, req *admin_service.ScheduleWeekPrimaryKey) (resp *admin_service.ScheduleWeek, err error) {

	query := `
		SELECT
			id,
			group_id,
			group_type,
			number_of_students,
			start_time,
			end_time,
			branch_id,
			teacher_id,
			support_teacher_id,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "schedule_week"
		WHERE id = $1
	`

	var (
		Id               sql.NullString
		GroupId          sql.NullString
		GroupType        sql.NullString
		NumberOfStudents sql.NullString
		StartTime        sql.NullString
		EndTime          sql.NullString
		BranchId         sql.NullString
		TeacherId        sql.NullString
		SupportTeacherId sql.NullString
		CreatedAt        sql.NullString
		UpdatedAt        sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&Id,
		&GroupId,
		&GroupType,
		&NumberOfStudents,
		&StartTime,
		&EndTime,
		&BranchId,
		&TeacherId,
		&SupportTeacherId,
		&CreatedAt,
		&UpdatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &admin_service.ScheduleWeek{
		Id:               Id.String,
		GroupId:          GroupId.String,
		GroupType:        GroupType.String,
		NumberOfStudents: NumberOfStudents.String,
		StartTime:        StartTime.String,
		EndTime:          EndTime.String,
		BranchId:         BranchId.String,
		TeacherId:        TeacherId.String,
		SupportTeacherId: SupportTeacherId.String,
		CreatedAt:        CreatedAt.String,
		UpdatedAt:        UpdatedAt.String,
	}

	return
}

func (c *scheduleWeekRepo) GetAll(ctx context.Context, req *admin_service.GetListScheduleWeekRequest) (resp *admin_service.GetListScheduleWeekResponse, err error) {

	resp = &admin_service.GetListScheduleWeekResponse{}

	var (
		query  string
		limit  = " LIMIT ALL"
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			group_id,
			group_type,
			number_of_students,
			start_time,
			end_time,
			branch_id,
			teacher_id,
			support_teacher_id,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "schedule_week"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	if len(req.Search) > 0 {
		query += " WHERE group_type ILIKE '" + req.Search + "' OR group_type ILIKE '%" + req.Search + "' OR group_type ILIKE '" + req.Search + "%'"
	} else {
		query += filter
	}
	query += sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	if err != nil {
		return resp, err
	}
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			Id               sql.NullString
			GroupId          sql.NullString
			GroupType        sql.NullString
			NumberOfStudents sql.NullString
			StartTime        sql.NullString
			EndTime          sql.NullString
			BranchId         sql.NullString
			TeacherId        sql.NullString
			SupportTeacherId sql.NullString
			CreatedAt        sql.NullString
			UpdatedAt        sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&Id,
			&GroupId,
			&GroupType,
			&NumberOfStudents,
			&StartTime,
			&EndTime,
			&BranchId,
			&TeacherId,
			&SupportTeacherId,
			&CreatedAt,
			&UpdatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.ScheduleWeeks = append(resp.ScheduleWeeks, &admin_service.ScheduleWeek{
			Id:               Id.String,
			GroupId:          GroupId.String,
			GroupType:        GroupType.String,
			NumberOfStudents: NumberOfStudents.String,
			StartTime:        StartTime.String,
			EndTime:          EndTime.String,
			BranchId:         BranchId.String,
			TeacherId:        TeacherId.String,
			SupportTeacherId: SupportTeacherId.String,
			CreatedAt:        CreatedAt.String,
			UpdatedAt:        UpdatedAt.String,
		})
	}

	return
}

func (c *scheduleWeekRepo) Update(ctx context.Context, req *admin_service.UpdateScheduleWeek) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "schedule_week"
			SET
				id = :id,
				group_id = :group_id,
				group_type = :group_type,
				number_of_students = :number_of_students,
				start_time = :start_time,
				end_time = :end_time,
				branch_id = :branch_id,
				teacher_id = :teacher_id,
				support_teacher_id = :support_teacher_id,
				updated_at = NOW()
			WHERE
				id = :id`
	params = map[string]interface{}{
		"id":                 req.GetId(),
		"group_id":           req.GetGroupId(),
		"group_type":         req.GetGroupType(),
		"number_of_students": req.GetNumberOfStudents(),
		"start_time":         req.GetStartTime(),
		"end_time":           req.GetEndTime(),
		"branch_id":          req.GetBranchId(),
		"teacher_id":         req.GetTeacherId(),
		"support_teacher_id": req.GetSupportTeacherId(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *scheduleWeekRepo) Delete(ctx context.Context, req *admin_service.ScheduleWeekPrimaryKey) error {

	query := `DELETE FROM "schedule_week" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return err
	}

	return nil
}
