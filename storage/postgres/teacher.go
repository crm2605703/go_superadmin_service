package memory

import (
	"admin_service/genproto/admin_service"
	"admin_service/packages/helper"
	"admin_service/storage"
	"context"
	"database/sql"
	"strconv"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type teacherRepo struct {
	db *pgxpool.Pool
}

func NewTeacherRepo(db *pgxpool.Pool) storage.TeacherRepoI {
	return &teacherRepo{
		db: db,
	}
}

func (c *teacherRepo) Create(ctx context.Context, req *admin_service.CreateTeacher) (resp *admin_service.TeacherPrimaryKey, err error) {

	var (
		id           = uuid.New().String()
		increment_id = ""
		maxQuery     = `SELECT MAX(increment_id) FROM "teacher"`
		digit        = 0
	)

	err = c.db.QueryRow(ctx, maxQuery).Scan(&increment_id)
	if err != nil {
		if err.Error() != "can't scan into dest[0]: cannot scan null into *string" && err.Error() != "no rows in result set" {
			return resp, err
		} else {
			increment_id = "T0000000"
		}
	}
	if len(increment_id) > 2 {
		digit, err = strconv.Atoi(increment_id[2:])
		if err != nil {
			return resp, err
		}
	}

	query := `
		INSERT INTO "teacher" (
			id,
			increment_id,
			branch_id,
			login,
			password,
			name,
			surname,
			phone,
			salary,
			attempts,
			month_worked,
			total_sum,
			ielts_score,
			updated_at
			) VALUES
			($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, NOW())
		`

	_, err = c.db.Exec(ctx,
		query,
		id,
		"T"+helper.GetSerialId(digit),
		req.BranchId,
		req.Login,
		req.Password,
		req.Name,
		req.Surname,
		req.Phone,
		req.Salary,
		req.Attempts,
		req.MonthWorked,
		req.TotalSum,
		req.IeltsScore,
	)

	if err != nil {
		return nil, err
	}

	return &admin_service.TeacherPrimaryKey{Id: id}, nil
}

func (c *teacherRepo) GetByPKey(ctx context.Context, req *admin_service.TeacherPrimaryKey) (resp *admin_service.Teacher, err error) {

	query := `
		SELECT
			id,
			increment_id,
			branch_id,
			login,
			password,
			name,
			surname,
			phone,
			salary,
			attempts,
			month_worked,
			total_sum,
			ielts_score,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "teacher"
		WHERE id = $1
	`

	var (
		Id          sql.NullString
		IncrementId sql.NullString
		BranchId    sql.NullString
		Login       sql.NullString
		Password    sql.NullString
		Name        sql.NullString
		Surname     sql.NullString
		Phone       sql.NullString
		Salary      sql.NullFloat64
		Attempts    sql.NullInt64
		MonthWorked sql.NullInt64
		TotalSum    sql.NullFloat64
		IeltsScore  sql.NullFloat64
		CreatedAt   sql.NullString
		UpdatedAt   sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&Id,
		&IncrementId,
		&BranchId,
		&Login,
		&Password,
		&Name,
		&Surname,
		&Phone,
		&Salary,
		&Attempts,
		&MonthWorked,
		&TotalSum,
		&IeltsScore,
		&CreatedAt,
		&UpdatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &admin_service.Teacher{
		Id:          Id.String,
		IncrementId: IncrementId.String,
		BranchId:    BranchId.String,
		Login:       Login.String,
		Password:    Password.String,
		Name:        Name.String,
		Surname:     Surname.String,
		Phone:       Phone.String,
		Salary:      float32(Salary.Float64),
		Attempts:    Attempts.Int64,
		MonthWorked: MonthWorked.Int64,
		TotalSum:    float32(TotalSum.Float64),
		IeltsScore:  float32(IeltsScore.Float64),
		CreatedAt:   CreatedAt.String,
		UpdatedAt:   UpdatedAt.String,
	}

	return
}

func (c *teacherRepo) GetAll(ctx context.Context, req *admin_service.GetListTeacherRequest) (resp *admin_service.GetListTeacherResponse, err error) {

	resp = &admin_service.GetListTeacherResponse{}

	var (
		query  string
		limit  = " LIMIT ALL"
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			increment_id,
			branch_id,
			login,
			password,
			name,
			surname,
			phone,
			salary,
			attempts,
			month_worked,
			total_sum,
			ielts_score,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "teacher"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	if len(req.Search) > 0 {
		query += " WHERE increment_id ILIKE '" + req.Search + "' OR increment_id ILIKE '%" + req.Search + "' OR increment_id ILIKE '" + req.Search + "%'"
		query += " OR name ILIKE '" + req.Search + "' OR name ILIKE '%" + req.Search + "' OR name ILIKE '" + req.Search + "%'"
		query += " OR surname ILIKE '" + req.Search + "' OR surname ILIKE '%" + req.Search + "' OR surname ILIKE '" + req.Search + "%'"
		query += " OR phone ILIKE '" + req.Search + "' OR phone ILIKE '%" + req.Search + "' OR phone ILIKE '" + req.Search + "%'"
		query += " OR salary ILIKE '" + req.Search + "' OR salary ILIKE '%" + req.Search + "' OR salary ILIKE '" + req.Search + "%'"
	} else {
		query += filter
	}
	query += sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	if err != nil {
		return resp, err
	}
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			Id          sql.NullString
			IncrementId sql.NullString
			BranchId    sql.NullString
			Login       sql.NullString
			Password    sql.NullString
			Name        sql.NullString
			Surname     sql.NullString
			Phone       sql.NullString
			Salary      sql.NullFloat64
			Attempts    sql.NullInt64
			MonthWorked sql.NullInt64
			TotalSum    sql.NullFloat64
			IeltsScore  sql.NullFloat64
			CreatedAt   sql.NullString
			UpdatedAt   sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&Id,
			&IncrementId,
			&BranchId,
			&Login,
			&Password,
			&Name,
			&Surname,
			&Phone,
			&Salary,
			&Attempts,
			&MonthWorked,
			&TotalSum,
			&IeltsScore,
			&CreatedAt,
			&UpdatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Teachers = append(resp.Teachers, &admin_service.Teacher{
			Id:          Id.String,
			IncrementId: IncrementId.String,
			BranchId:    BranchId.String,
			Login:       Login.String,
			Password:    Password.String,
			Name:        Name.String,
			Surname:     Surname.String,
			Phone:       Phone.String,
			Salary:      float32(Salary.Float64),
			Attempts:    Attempts.Int64,
			MonthWorked: MonthWorked.Int64,
			TotalSum:    float32(TotalSum.Float64),
			IeltsScore:  float32(IeltsScore.Float64),
			CreatedAt:   CreatedAt.String,
			UpdatedAt:   UpdatedAt.String,
		})
	}

	return
}

func (c *teacherRepo) Update(ctx context.Context, req *admin_service.UpdateTeacher) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "teacher"
			SET
				id = :id,
				branch_id = :branch_id,
				login = :login,
				password = :password,
				name = :name,
				surname = :surname,
				phone = :phone,
				salary = :salary,
				attempts = :attempts,
				month_worked = :month_worked,
				total_sum = :total_sum,
				ielts_score = :ielts_score,
				updated_at = NOW()
			WHERE
				id = :id`
	params = map[string]interface{}{
		"id":           req.GetId(),
		"branch_id":    req.GetBranchId(),
		"login":        req.GetLogin(),
		"password":     req.GetPassword(),
		"name":         req.GetName(),
		"surname":      req.GetSurname(),
		"phone":        req.GetPhone(),
		"salary":       req.GetSalary(),
		"attempts":     req.GetAttempts(),
		"month_worked": req.GetMonthWorked(),
		"total_sum":    req.GetTotalSum(),
		"ielts_score":  req.GetIeltsScore(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *teacherRepo) Delete(ctx context.Context, req *admin_service.TeacherPrimaryKey) error {

	query := `DELETE FROM "teacher" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return err
	}

	return nil
}
