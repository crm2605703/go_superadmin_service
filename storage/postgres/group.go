package memory

import (
	"admin_service/genproto/admin_service"
	"admin_service/packages/helper"
	"admin_service/storage"
	"context"
	"database/sql"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type groupRepo struct {
	db *pgxpool.Pool
}

func NewGroupRepo(db *pgxpool.Pool) storage.GroupRepoI {
	return &groupRepo{
		db: db,
	}
}

func (c *groupRepo) Create(ctx context.Context, req *admin_service.CreateGroup) (resp *admin_service.GroupPrimaryKey, err error) {

	var (
		id = uuid.New().String()
	)

	query := `
		INSERT INTO "group" (
			id,
			group_type,
			teacher_id,
			branch_id,
			support_teacher_id,
			updated_at
			) VALUES
			($1, $2, $3, $4, $5,  NOW())
		`

	_, err = c.db.Exec(ctx,
		query,
		id,
		req.GroupType,
		req.TeacherId,
		req.BranchId,
		req.SupportTeacherId,
	)

	if err != nil {
		return nil, err
	}

	return &admin_service.GroupPrimaryKey{Id: id}, nil
}

func (c *groupRepo) GetByPKey(ctx context.Context, req *admin_service.GroupPrimaryKey) (resp *admin_service.Group, err error) {

	query := `
		SELECT
			id,
			group_type,
			teacher_id,
			branch_id,
			support_teacher_id,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "group"
		WHERE id = $1
	`

	var (
		Id               sql.NullString
		GroupType        sql.NullString
		TeacherId        sql.NullString
		BranchId         sql.NullString
		SupportTeacherId sql.NullString
		CreatedAt        sql.NullString
		UpdatedAt        sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&Id,
		&GroupType,
		&TeacherId,
		&BranchId,
		&SupportTeacherId,
		&CreatedAt,
		&UpdatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &admin_service.Group{
		Id:               Id.String,
		GroupType:        GroupType.String,
		TeacherId:        TeacherId.String,
		BranchId:         BranchId.String,
		SupportTeacherId: SupportTeacherId.String,
		CreatedAt:        CreatedAt.String,
		UpdatedAt:        UpdatedAt.String,
	}

	return
}

func (c *groupRepo) GetAll(ctx context.Context, req *admin_service.GetListGroupRequest) (resp *admin_service.GetListGroupResponse, err error) {

	resp = &admin_service.GetListGroupResponse{}

	var (
		query  string
		limit  = " LIMIT ALL"
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			group_type,
			teacher_id,
			branch_id,
			support_teacher_id,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "group"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	if len(req.Search) > 0 {
		query += " WHERE teacher_id ILIKE '" + req.Search + "' OR teacher_id ILIKE '%" + req.Search + "' OR teacher_id ILIKE '" + req.Search + "%'"
		query += " OR support_teacher_id ILIKE '" + req.Search + "' OR support_teacher_id ILIKE '%" + req.Search + "' OR support_teacher_id ILIKE '" + req.Search + "%'"
		query += " OR branch_id ILIKE '" + req.Search + "' OR branch_id ILIKE '%" + req.Search + "' OR branch_id ILIKE '" + req.Search + "%'"
		query += " OR group_type ILIKE '" + req.Search + "' OR group_type ILIKE '%" + req.Search + "' OR group_type ILIKE '" + req.Search + "%'"
	} else {
		query += filter
	}
	query += sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	if err != nil {
		return resp, err
	}
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			Id               sql.NullString
			GroupType        sql.NullString
			TeacherId        sql.NullString
			BranchId         sql.NullString
			SupportTeacherId sql.NullString
			CreatedAt        sql.NullString
			UpdatedAt        sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&Id,
			&GroupType,
			&TeacherId,
			&BranchId,
			&SupportTeacherId,
			&CreatedAt,
			&UpdatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Groups = append(resp.Groups, &admin_service.Group{
			Id:               Id.String,
			GroupType:        GroupType.String,
			TeacherId:        TeacherId.String,
			BranchId:         BranchId.String,
			SupportTeacherId: SupportTeacherId.String,
			CreatedAt:        CreatedAt.String,
			UpdatedAt:        UpdatedAt.String,
		})
	}

	return
}

func (c *groupRepo) Update(ctx context.Context, req *admin_service.UpdateGroup) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "group"
			SET
				id = :id,
				group_type = :group_type,
				teacher_id = :teacher_id,
				branch_id = :branch_id,
				support_teacher_id = :support_teacher_id,
				updated_at = NOW()
			WHERE
				id = :id`
	params = map[string]interface{}{
		"id":                 req.GetId(),
		"group_type":         req.GetGroupType(),
		"teacher_id":         req.GetTeacherId(),
		"branch_id":          req.GetBranchId(),
		"support_teacher_id": req.GetSupportTeacherId(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *groupRepo) Delete(ctx context.Context, req *admin_service.GroupPrimaryKey) error {

	query := `DELETE FROM "group" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return err
	}

	return nil
}
