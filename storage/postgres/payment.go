package memory

import (
	"admin_service/genproto/admin_service"
	"admin_service/packages/helper"
	"admin_service/storage"
	"context"
	"database/sql"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type paymentRepo struct {
	db *pgxpool.Pool
}

func NewPaymentRepo(db *pgxpool.Pool) storage.PaymentRepoI {
	return &paymentRepo{
		db: db,
	}
}

func (c *paymentRepo) Create(ctx context.Context, req *admin_service.CreatePayment) (resp *admin_service.PaymentPrimaryKey, err error) {

	var (
		id = uuid.New().String()
	)

	query := `
		INSERT INTO "payment" (
			id,
			branch_id,
			student_id,
			paid_sum,
			updated_at
			) VALUES
			($1, $2, $3, $4, NOW())
		`

	_, err = c.db.Exec(ctx,
		query,
		id,
		req.BranchId,
		req.StudentId,
		req.PaidSum,
	)

	if err != nil {
		return nil, err
	}

	return &admin_service.PaymentPrimaryKey{Id: id}, nil
}

func (c *paymentRepo) GetByPKey(ctx context.Context, req *admin_service.PaymentPrimaryKey) (resp *admin_service.Payment, err error) {

	query := `
		SELECT
			id,
			branch_id,
			student_id,
			paid_sum,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "payment"
		WHERE id = $1
	`

	var (
		Id        sql.NullString
		BranchId  sql.NullString
		StudentId sql.NullString
		PaidSum   sql.NullFloat64
		CreatedAt sql.NullString
		UpdatedAt sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&Id,
		&BranchId,
		&StudentId,
		&PaidSum,
		&CreatedAt,
		&UpdatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &admin_service.Payment{
		Id:        Id.String,
		BranchId:  BranchId.String,
		StudentId: StudentId.String,
		PaidSum:   float32(PaidSum.Float64),
		CreatedAt: CreatedAt.String,
		UpdatedAt: UpdatedAt.String,
	}

	return
}

func (c *paymentRepo) GetAll(ctx context.Context, req *admin_service.GetListPaymentRequest) (resp *admin_service.GetListPaymentResponse, err error) {

	resp = &admin_service.GetListPaymentResponse{}

	var (
		query  string
		limit  = " LIMIT ALL"
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			branch_id,
			student_id,
			paid_sum,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "payment"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	if len(req.Search) > 0 {
		query += " WHERE paid_sum ILIKE '" + req.Search + "' OR paid_sum ILIKE '%" + req.Search + "' OR paid_sum ILIKE '" + req.Search + "%'"
	} else {
		query += filter
	}
	query += sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	if err != nil {
		return resp, err
	}
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			Id        sql.NullString
			BranchId  sql.NullString
			StudentId sql.NullString
			PaidSum   sql.NullFloat64
			CreatedAt sql.NullString
			UpdatedAt sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&Id,
			&BranchId,
			&StudentId,
			&PaidSum,
			&CreatedAt,
			&UpdatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Payments = append(resp.Payments, &admin_service.Payment{
			Id:        Id.String,
			BranchId:  BranchId.String,
			StudentId: StudentId.String,
			PaidSum:   float32(PaidSum.Float64),
			CreatedAt: CreatedAt.String,
			UpdatedAt: UpdatedAt.String,
		})
	}

	return
}

func (c *paymentRepo) Update(ctx context.Context, req *admin_service.UpdatePayment) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "payment"
			SET
				id = :id,
				branch_id = :branch_id,
				student_id = :student_id,
				paid_sum = :paid_sum,
				updated_at = NOW()
			WHERE
				id = :id`
	params = map[string]interface{}{
		"id":         req.GetId(),
		"branch_id":  req.GetBranchId(),
		"student_id": req.GetStudentId(),
		"paid_sum":   req.GetPaidSum(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *paymentRepo) Delete(ctx context.Context, req *admin_service.PaymentPrimaryKey) error {

	query := `DELETE FROM "payment" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return err
	}

	return nil
}
