package memory

import (
	"admin_service/config"
	"admin_service/storage"
	"context"
	"fmt"
	"log"

	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
)

type Store struct {
	db             *pgxpool.Pool
	administration storage.AdministrationRepoI
	branch         storage.BranchRepoI
	event          storage.EventRepoI
	group          storage.GroupRepoI
	journal        storage.JournalRepoI
	manager        storage.ManagerRepoI
	scheduleWeek   storage.ScheduleWeekRepoI
	student        storage.StudentRepoI
	superAdmin     storage.SuperAdminRepoI
	supportTeacher storage.SupportTeacherRepoI
	task           storage.TaskRepoI
	teacher        storage.TeacherRepoI
	lesson         storage.LessonRepoI
	toDoTask       storage.ToDoTaskRepoI
	assignStudent  storage.AssignStudentRepoI
	userBranch     storage.UserBranchRepoI
	payment        storage.PaymentRepoI
	report         storage.ReportRepoI
}

// type Pool struct {
// 	db *pgxpool.Pool
// }

func NewPostgres(ctx context.Context, cfg config.Config) (storage.StorageI, error) {
	config, err := pgxpool.ParseConfig(fmt.Sprintf(
		"postgres://%s:%s@%s:%d/%s?sslmode=disable",
		cfg.PostgresUser,
		cfg.PostgresPassword,
		cfg.PostgresHost,
		cfg.PostgresPort,
		cfg.PostgresDatabase,
	))
	if err != nil {
		return nil, err
	}

	config.MaxConns = cfg.PostgresMaxConnections

	pool, err := pgxpool.ConnectConfig(ctx, config)
	if err != nil {
		return nil, err
	}

	return &Store{
		db: pool,
	}, err
}

func (s *Store) CloseDB() {
	s.db.Close()
}

func (l *Store) Log(ctx context.Context, level pgx.LogLevel, msg string, data map[string]interface{}) {
	args := make([]interface{}, 0, len(data)+2) // making space for arguments + level + msg
	args = append(args, level, msg)
	for k, v := range data {
		args = append(args, fmt.Sprintf("%s=%v", k, v))
	}
	log.Println(args...)
}

func (s *Store) Report() storage.ReportRepoI {
	if s.report == nil {
		s.report = NewReportRepo(s.db)
	}

	return s.report
}

func (s *Store) Payment() storage.PaymentRepoI {
	if s.payment == nil {
		s.payment = NewPaymentRepo(s.db)
	}

	return s.payment
}

func (s *Store) UserBranch() storage.UserBranchRepoI {
	if s.userBranch == nil {
		s.userBranch = NewUserBranchRepo(s.db)
	}

	return s.userBranch
}

func (s *Store) AssignStudent() storage.AssignStudentRepoI {
	if s.assignStudent == nil {
		s.assignStudent = NewAssignStudentRepo(s.db)
	}

	return s.assignStudent
}

func (s *Store) ToDoTask() storage.ToDoTaskRepoI {
	if s.toDoTask == nil {
		s.toDoTask = NewToDoTaskRepo(s.db)
	}

	return s.toDoTask
}

func (s *Store) Lesson() storage.LessonRepoI {
	if s.lesson == nil {
		s.lesson = NewLessonRepo(s.db)
	}

	return s.lesson
}

func (s *Store) Administration() storage.AdministrationRepoI {
	if s.administration == nil {
		s.administration = NewAdministrationRepo(s.db)
	}

	return s.administration
}

func (s *Store) Branch() storage.BranchRepoI {
	if s.branch == nil {
		s.branch = NewBranchRepo(s.db)
	}

	return s.branch
}

func (s *Store) Event() storage.EventRepoI {
	if s.event == nil {
		s.event = NewEventRepo(s.db)
	}

	return s.event
}

func (s *Store) Group() storage.GroupRepoI {
	if s.group == nil {
		s.group = NewGroupRepo(s.db)
	}

	return s.group
}

func (s *Store) Journal() storage.JournalRepoI {
	if s.journal == nil {
		s.journal = NewJournalRepo(s.db)
	}

	return s.journal
}

func (s *Store) Manager() storage.ManagerRepoI {
	if s.manager == nil {
		s.manager = NewManagerRepo(s.db)
	}

	return s.manager
}

func (s *Store) ScheduleWeek() storage.ScheduleWeekRepoI {
	if s.scheduleWeek == nil {
		s.scheduleWeek = NewScheduleWeekRepo(s.db)
	}

	return s.scheduleWeek
}

func (s *Store) Student() storage.StudentRepoI {
	if s.student == nil {
		s.student = NewStudentRepo(s.db)
	}

	return s.student
}

func (s *Store) SuperAdmin() storage.SuperAdminRepoI {
	if s.superAdmin == nil {
		s.superAdmin = NewSuperAdminRepo(s.db)
	}

	return s.superAdmin
}

func (s *Store) SupportTeacher() storage.SupportTeacherRepoI {
	if s.supportTeacher == nil {
		s.supportTeacher = NewSupportTeacherRepo(s.db)
	}

	return s.supportTeacher
}

func (s *Store) Task() storage.TaskRepoI {
	if s.task == nil {
		s.task = NewTaskRepo(s.db)
	}

	return s.task
}

func (s *Store) Teacher() storage.TeacherRepoI {
	if s.teacher == nil {
		s.teacher = NewTeacherRepo(s.db)
	}

	return s.teacher
}
