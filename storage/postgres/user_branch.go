package memory

import (
	"admin_service/genproto/admin_service"
	"admin_service/packages/helper"
	"admin_service/storage"
	"context"
	"database/sql"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type userBranchRepo struct {
	db *pgxpool.Pool
}

func NewUserBranchRepo(db *pgxpool.Pool) storage.UserBranchRepoI {
	return &userBranchRepo{
		db: db,
	}
}

func (c *userBranchRepo) Create(ctx context.Context, req *admin_service.CreateUserBranch) (resp *admin_service.UserBranchPrimaryKey, err error) {

	var (
		id = uuid.New().String()
	)

	query := `
		INSERT INTO "user_branch" (
			id, 
			branch_id, 
			login,
			password,
			updated_at
		) VALUES 
			($1, $2, $3, $4, NOW())`

	_, err = c.db.Exec(ctx,
		query,
		id,
		req.BranchId,
		req.Login,
		req.Password,
	)

	if err != nil {
		return nil, err
	}

	return &admin_service.UserBranchPrimaryKey{Login: req.Login, Password: req.Password}, nil
}

func (c *userBranchRepo) GetByPKey(ctx context.Context, req *admin_service.UserBranchPrimaryKey) (resp *admin_service.UserBranch, err error) {

	query := `
		SELECT
			id,
			branch_id,
			login,
			password,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "user_branch"
		WHERE login = $1 AND password = $2
	`
	var (
		Id        sql.NullString
		BranchId  sql.NullString
		Login     sql.NullString
		Password  sql.NullString
		CreatedAt sql.NullString
		UpdatedAt sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Login, req.Password).Scan(
		&Id,
		&BranchId,
		&Login,
		&Password,
		&CreatedAt,
		&UpdatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &admin_service.UserBranch{
		Id:        Id.String,
		BranchId:  BranchId.String,
		Login:     Login.String,
		Password:  Password.String,
		CreatedAt: CreatedAt.String,
		UpdatedAt: UpdatedAt.String,
	}

	return
}

func (c *userBranchRepo) GetAll(ctx context.Context, req *admin_service.GetListUserBranchRequest) (resp *admin_service.GetListUserBranchResponse, err error) {

	resp = &admin_service.GetListUserBranchResponse{}

	var (
		query  string
		limit  = " LIMIT ALL"
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			branch_id,
			login,
			password,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "user_branch"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	if len(req.Search) > 0 {
		query += " WHERE branch_id ILIKE '" + req.Search + "' OR branch_id ILIKE '%" + req.Search + "' OR branch_id ILIKE '" + req.Search + "%'"
		query += " OR login ILIKE '" + req.Search + "' OR login ILIKE '%" + req.Search + "' OR login ILIKE '" + req.Search + "%'"
		query += " OR password ILIKE '" + req.Search + "' OR password ILIKE '%" + req.Search + "' OR password ILIKE '" + req.Search + "%'"
	} else {
		query += filter
	}
	query += sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	if err != nil {
		return resp, err
	}
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			Id        sql.NullString
			BranchId  sql.NullString
			Login     sql.NullString
			Password  sql.NullString
			CreatedAt sql.NullString
			UpdatedAt sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&Id,
			&BranchId,
			&Login,
			&Password,
			&CreatedAt,
			&UpdatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.UserBranchs = append(resp.UserBranchs, &admin_service.UserBranch{
			Id:        Id.String,
			BranchId:  BranchId.String,
			Login:     Login.String,
			Password:  Password.String,
			CreatedAt: CreatedAt.String,
			UpdatedAt: UpdatedAt.String,
		})
	}

	return
}

func (c *userBranchRepo) Update(ctx context.Context, req *admin_service.UpdateUserBranch) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "user_branch"
			SET
				id = :id,
				branch_id = :branch_id,
				login = :login,
				password = :password,
				updated_at = NOW()
			WHERE
				id = :id`
	params = map[string]interface{}{
		"id":        req.GetId(),
		"branch_id": req.GetBranchId(),
		"login":     req.GetLogin(),
		"password":  req.GetPassword(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *userBranchRepo) Delete(ctx context.Context, req *admin_service.UserBranchPrimaryKey) error {

	query := `DELETE FROM "user_branch" WHERE login = $1 AND password = $2`

	_, err := c.db.Exec(ctx, query, req.Login, req.Password)

	if err != nil {
		return err
	}

	return nil
}
