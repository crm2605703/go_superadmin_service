package memory

import (
	"admin_service/genproto/admin_service"
	"admin_service/packages/helper"
	"admin_service/storage"
	"context"
	"database/sql"
	"strconv"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type superAdminRepo struct {
	db *pgxpool.Pool
}

func NewSuperAdminRepo(db *pgxpool.Pool) storage.SuperAdminRepoI {
	return &superAdminRepo{
		db: db,
	}
}

func (c *superAdminRepo) Create(ctx context.Context, req *admin_service.CreateSuperAdmin) (resp *admin_service.SuperAdminPrimaryKey, err error) {

	var (
		id           = uuid.New().String()
		increment_id = ""
		maxQuery     = `SELECT MAX(increment_id) FROM "super_admin"`
		digit        = 0
	)

	err = c.db.QueryRow(ctx, maxQuery).Scan(&increment_id)
	if err != nil {
		if err.Error() != "can't scan into dest[0]: cannot scan null into *string" && err.Error() != "no rows in result set" {
			return resp, err
		} else {
			increment_id = "SA0000000"
		}
	}
	if len(increment_id) > 2 {
		digit, err = strconv.Atoi(increment_id[2:])
		if err != nil {
			return resp, err
		}
	}

	query := `
		INSERT INTO "super_admin" (
			id,
			increment_id,
			branch_id,
			name,
			surname,
			login,
			password,
			updated_at
			) VALUES
			($1, $2, $3, $4, $5, $6, $7, NOW())
		`

	_, err = c.db.Exec(ctx,
		query,
		id,
		"SA"+helper.GetSerialId(digit),
		req.BranchId,
		req.Name,
		req.Surname,
		req.Login,
		req.Password,
	)

	if err != nil {
		return nil, err
	}

	return &admin_service.SuperAdminPrimaryKey{Id: id}, nil
}

func (c *superAdminRepo) GetByPKey(ctx context.Context, req *admin_service.SuperAdminPrimaryKey) (resp *admin_service.SuperAdmin, err error) {

	query := `
		SELECT
			id,
			increment_id,
			branch_id,
			name,
			surname,
			login,
			password,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "super_admin"
		WHERE id = $1
	`

	var (
		Id          sql.NullString
		IncrementId sql.NullString
		BranchId    sql.NullString
		Name        sql.NullString
		Surname     sql.NullString
		Login       sql.NullString
		Password    sql.NullString
		CreatedAt   sql.NullString
		UpdatedAt   sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&Id,
		&IncrementId,
		&BranchId,
		&Name,
		&Surname,
		&Login,
		&Password,
		&CreatedAt,
		&UpdatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &admin_service.SuperAdmin{
		Id:          Id.String,
		IncrementId: IncrementId.String,
		BranchId:    BranchId.String,
		Name:        Name.String,
		Surname:     Surname.String,
		Login:       Login.String,
		Password:    Password.String,
		CreatedAt:   CreatedAt.String,
		UpdatedAt:   UpdatedAt.String,
	}
	
	return
}

func (c *superAdminRepo) GetAll(ctx context.Context, req *admin_service.GetListSuperAdminRequest) (resp *admin_service.GetListSuperAdminResponse, err error) {

	resp = &admin_service.GetListSuperAdminResponse{}

	var (
		query  string
		limit  = " LIMIT ALL"
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			increment_id,
			branch_id,
			name,
			surname,
			login,
			password,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "super_admin"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	if len(req.Search) > 0 {
		query += " WHERE increment_id ILIKE '" + req.Search + "' OR increment_id ILIKE '%" + req.Search + "' OR increment_id ILIKE '" + req.Search + "%'"
		query += " OR name ILIKE '" + req.Search + "' OR name ILIKE '%" + req.Search + "' OR name ILIKE '" + req.Search + "%'"
		query += " OR surname ILIKE '" + req.Search + "' OR surname ILIKE '%" + req.Search + "' OR surname ILIKE '" + req.Search + "%'"
	} else {
		query += filter
	}
	query += sort + offset + limit
	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	if err != nil {
		return resp, err
	}
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			Id          sql.NullString
			IncrementId sql.NullString
			BranchId    sql.NullString
			Name        sql.NullString
			Surname     sql.NullString
			Login       sql.NullString
			Password    sql.NullString
			CreatedAt   sql.NullString
			UpdatedAt   sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&Id,
			&IncrementId,
			&BranchId,
			&Name,
			&Surname,
			&Login,
			&Password,
			&CreatedAt,
			&UpdatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.SuperAdmins = append(resp.SuperAdmins, &admin_service.SuperAdmin{
			Id:          Id.String,
			IncrementId: IncrementId.String,
			BranchId:    BranchId.String,
			Name:        Name.String,
			Surname:     Surname.String,
			Login:       Login.String,
			Password:    Password.String,
			CreatedAt:   CreatedAt.String,
			UpdatedAt:   UpdatedAt.String,
		})
	}

	return
}

func (c *superAdminRepo) Update(ctx context.Context, req *admin_service.UpdateSuperAdmin) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "super_admin"
			SET
				id = :id,
				branch_id = :branch_id,
				name = :name,
				surname = :surname,
				login = :login,
				password = :password,
				updated_at = NOW()
			WHERE
				id = :id`
	params = map[string]interface{}{
		"id":        req.GetId(),
		"branch_id": req.GetBranchId(),
		"name":      req.GetName(),
		"surname":   req.GetSurname(),
		"login":     req.GetLogin(),
		"password":  req.GetPassword(),
	}
	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *superAdminRepo) Delete(ctx context.Context, req *admin_service.SuperAdminPrimaryKey) error {

	query := `DELETE FROM "super_admin" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return err
	}

	return nil
}
