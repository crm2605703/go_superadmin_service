package memory

import (
	"admin_service/genproto/admin_service"
	"admin_service/packages/helper"
	"admin_service/storage"
	"context"
	"database/sql"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type eventRepo struct {
	db *pgxpool.Pool
}

func NewEventRepo(db *pgxpool.Pool) storage.EventRepoI {
	return &eventRepo{
		db: db,
	}
}

func (c *eventRepo) Create(ctx context.Context, req *admin_service.CreateEvent) (resp *admin_service.EventPrimaryKey, err error) {

	var (
		id = uuid.New().String()
	)

	query := `
		INSERT INTO "event" (
			id,
			branch_id,
			topic,
			start_time,
			day,
			updated_at
			) VALUES
			($1, $2, $3, $4, $5, NOW())
		`

	_, err = c.db.Exec(ctx,
		query,
		id,
		req.BranchId,
		req.Topic,
		req.StartTime,
		req.Day,
	)

	if err != nil {
		return nil, err
	}

	return &admin_service.EventPrimaryKey{Id: id}, nil
}

func (c *eventRepo) GetByPKey(ctx context.Context, req *admin_service.EventPrimaryKey) (resp *admin_service.Event, err error) {

	query := `
		SELECT
			id,
			branch_id,
			topic,
			start_time,
			TO_CHAR(day, 'YYYY-MM-DD'),
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "event"
		WHERE id = $1
	`

	var (
		Id        sql.NullString
		BranchId  sql.NullString
		Topic     sql.NullString
		StartTime sql.NullString
		Day       sql.NullString
		CreatedAt sql.NullString
		UpdatedAt sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&Id,
		&BranchId,
		&Topic,
		&StartTime,
		&Day,
		&CreatedAt,
		&UpdatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &admin_service.Event{
		Id:        Id.String,
		BranchId:  BranchId.String,
		Topic:     Topic.String,
		StartTime: StartTime.String,
		Day:       Day.String,
		CreatedAt: CreatedAt.String,
		UpdatedAt: UpdatedAt.String,
	}

	return
}

func (c *eventRepo) GetAll(ctx context.Context, req *admin_service.GetListEventRequest) (resp *admin_service.GetListEventResponse, err error) {

	resp = &admin_service.GetListEventResponse{}

	var (
		query  string
		limit  = " LIMIT ALL"
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			branch_id,
			topic,
			start_time,
			TO_CHAR(day, 'YYYY-MM-DD'),
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "event"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	if len(req.Search) > 0 {
		query += " WHERE topic ILIKE '" + req.Search + "' OR topic ILIKE '%" + req.Search + "' OR topic ILIKE '" + req.Search + "%'"
		query += " OR branch_id ILIKE '" + req.Search + "' OR branch_id ILIKE '%" + req.Search + "' OR branch_id ILIKE '" + req.Search + "%'"
		query += " OR day ILIKE '" + req.Search + "' OR day ILIKE '%" + req.Search + "' OR day ILIKE '" + req.Search + "%'"
	} else {
		query += filter
	}
	query += sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	if err != nil {
		return resp, err
	}
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			Id        sql.NullString
			BranchId  sql.NullString
			Topic     sql.NullString
			StartTime sql.NullString
			Day       sql.NullString
			CreatedAt sql.NullString
			UpdatedAt sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&Id,
			&BranchId,
			&Topic,
			&StartTime,
			&Day,
			&CreatedAt,
			&UpdatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Events = append(resp.Events, &admin_service.Event{
			Id:        Id.String,
			BranchId:  BranchId.String,
			Topic:     Topic.String,
			StartTime: StartTime.String,
			Day:       Day.String,
			CreatedAt: CreatedAt.String,
			UpdatedAt: UpdatedAt.String,
		})
	}

	return
}

func (c *eventRepo) Update(ctx context.Context, req *admin_service.UpdateEvent) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "event"
			SET
				id = :id,
				branch_id = :branch_id,
				topic = :topic,
				start_time = :start_time,
				day = :day,
				updated_at = NOW()
			WHERE
				id = :id`
	params = map[string]interface{}{
		"id":         req.GetId(),
		"branch_id":  req.GetBranchId(),
		"topic":      req.GetTopic(),
		"start_time": req.GetStartTime(),
		"day":        req.GetDay(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *eventRepo) Delete(ctx context.Context, req *admin_service.EventPrimaryKey) error {

	query := `DELETE FROM "event" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return err
	}

	return nil
}
