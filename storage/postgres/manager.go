package memory

import (
	"admin_service/genproto/admin_service"
	"admin_service/packages/helper"
	"admin_service/storage"
	"context"
	"database/sql"
	"strconv"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type managerRepo struct {
	db *pgxpool.Pool
}

func NewManagerRepo(db *pgxpool.Pool) storage.ManagerRepoI {
	return &managerRepo{
		db: db,
	}
}

func (c *managerRepo) Create(ctx context.Context, req *admin_service.CreateManager) (resp *admin_service.ManagerPrimaryKey, err error) {

	var (
		id           = uuid.New().String()
		increment_id = ""
		maxQuery     = `SELECT MAX(increment_id) FROM "manager"`
		digit        = 0
	)

	err = c.db.QueryRow(ctx, maxQuery).Scan(&increment_id)
	if err != nil {
		if err.Error() != "can't scan into dest[0]: cannot scan null into *string" && err.Error() != "no rows in result set" {
			return resp, err
		} else {
			increment_id = "M0000000"
		}
	}
	if len(increment_id) > 2 {
		digit, err = strconv.Atoi(increment_id[2:])
		if err != nil {
			return resp, err
		}
	}

	query := `
		INSERT INTO "manager" (
			id,
			increment_id,
			name,
			surname,
			phone,
			login,
			password,
			salary,
			branch_id,
			updated_at
			) VALUES
			($1, $2, $3, $4, $5, $6, $7, $8, $9, NOW())
		`

	_, err = c.db.Exec(ctx,
		query,
		id,
		"M"+helper.GetSerialId(digit),
		req.Name,
		req.Surname,
		req.Phone,
		req.Login,
		req.Password,
		req.Salary,
		req.BranchId,
	)

	if err != nil {
		return nil, err
	}

	return &admin_service.ManagerPrimaryKey{Id: id}, nil
}

func (c *managerRepo) GetByPKey(ctx context.Context, req *admin_service.ManagerPrimaryKey) (resp *admin_service.Manager, err error) {

	query := `
		SELECT
			id,
			increment_id,
			name,
			surname,
			phone,
			login,
			password,
			salary,
			branch_id,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "manager"
		WHERE id = $1
	`

	var (
		Id          sql.NullString
		IncrementId sql.NullString
		Name        sql.NullString
		Surname     sql.NullString
		Phone       sql.NullString
		Login       sql.NullString
		Password    sql.NullString
		Salary      sql.NullFloat64
		BranchId    sql.NullString
		CreatedAt   sql.NullString
		UpdatedAt   sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&Id,
		&IncrementId,
		&Name,
		&Surname,
		&Phone,
		&Login,
		&Password,
		&Salary,
		&BranchId,
		&CreatedAt,
		&UpdatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &admin_service.Manager{
		Id:          Id.String,
		IncrementId: IncrementId.String,
		Name:        Name.String,
		Surname:     Surname.String,
		Phone:       Phone.String,
		Login:       Login.String,
		Password:    Password.String,
		Salary:      float32(Salary.Float64),
		BranchId:    BranchId.String,
		CreatedAt:   CreatedAt.String,
		UpdatedAt:   UpdatedAt.String,
	}

	return
}

func (c *managerRepo) GetAll(ctx context.Context, req *admin_service.GetListManagerRequest) (resp *admin_service.GetListManagerResponse, err error) {

	resp = &admin_service.GetListManagerResponse{}

	var (
		query  string
		limit  = " LIMIT ALL"
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			increment_id,
			name,
			surname,
			phone,
			login,
			password,
			salary,
			branch_id,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "manager"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	if len(req.Search) > 0 {
		query += " WHERE increment_id ILIKE '" + req.Search + "' OR increment_id ILIKE '%" + req.Search + "' OR increment_id ILIKE '" + req.Search + "%'"
		query += " OR name ILIKE '" + req.Search + "' OR name ILIKE '%" + req.Search + "' OR name ILIKE '" + req.Search + "%'"
		query += " OR surname ILIKE '" + req.Search + "' OR surname ILIKE '%" + req.Search + "' OR surname ILIKE '" + req.Search + "%'"
		query += " OR phone ILIKE '" + req.Search + "' OR phone ILIKE '%" + req.Search + "' OR phone ILIKE '" + req.Search + "%'"
		query += " OR branch_id ILIKE '" + req.Search + "' OR branch_id ILIKE '%" + req.Search + "' OR branch_id ILIKE '" + req.Search + "%'"
	} else {
		query += filter
	}
	query += sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	if err != nil {
		return resp, err
	}
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			Id          sql.NullString
			IncrementId sql.NullString
			Name        sql.NullString
			Surname     sql.NullString
			Phone       sql.NullString
			Login       sql.NullString
			Password    sql.NullString
			Salary      sql.NullFloat64
			BranchId    sql.NullString
			CreatedAt   sql.NullString
			UpdatedAt   sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&Id,
			&IncrementId,
			&Name,
			&Surname,
			&Phone,
			&Login,
			&Password,
			&Salary,
			&BranchId,
			&CreatedAt,
			&UpdatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Managers = append(resp.Managers, &admin_service.Manager{
			Id:          Id.String,
			IncrementId: IncrementId.String,
			Name:        Name.String,
			Surname:     Surname.String,
			Phone:       Phone.String,
			Login:       Login.String,
			Password:    Password.String,
			Salary:      float32(Salary.Float64),
			BranchId:    BranchId.String,
			CreatedAt:   CreatedAt.String,
			UpdatedAt:   UpdatedAt.String,
		})
	}

	return
}

func (c *managerRepo) Update(ctx context.Context, req *admin_service.UpdateManager) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "manager"
			SET
				id = :id,
				name = :name,
				surname = :surname,
				phone = :phone,
				login = :login,
				password = :password,
				salary = :salary,
				branch_id = :branch_id,
				updated_at = NOW()
			WHERE
				id = :id`
	params = map[string]interface{}{
		"id":        req.GetId(),
		"name":      req.GetName(),
		"surname":   req.GetSurname(),
		"phone":     req.GetPhone(),
		"login":     req.GetLogin(),
		"password":  req.GetPassword(),
		"salary":    req.GetSalary(),
		"branch_id": req.GetBranchId(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *managerRepo) Delete(ctx context.Context, req *admin_service.ManagerPrimaryKey) error {

	query := `DELETE FROM "manager" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return err
	}

	return nil
}
