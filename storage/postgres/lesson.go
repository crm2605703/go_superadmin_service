package memory

import (
	"admin_service/genproto/admin_service"
	"admin_service/packages/helper"
	"admin_service/storage"
	"context"
	"database/sql"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type lessonRepo struct {
	db *pgxpool.Pool
}

func NewLessonRepo(db *pgxpool.Pool) storage.LessonRepoI {
	return &lessonRepo{
		db: db,
	}
}

func (c *lessonRepo) Create(ctx context.Context, req *admin_service.CreateLesson) (resp *admin_service.LessonPrimaryKey, err error) {

	var (
		id = uuid.New().String()
	)

	query := `
		INSERT INTO "lesson" (
			id,
			journal_id,
			label,
			updated_at
			) VALUES
			($1, $2, $3, NOW())
		`

	_, err = c.db.Exec(ctx,
		query,
		id,
		req.JournalId,
		req.Label,
	)

	if err != nil {
		return nil, err
	}

	return &admin_service.LessonPrimaryKey{Id: id}, nil
}

func (c *lessonRepo) GetByPKey(ctx context.Context, req *admin_service.LessonPrimaryKey) (resp *admin_service.Lesson, err error) {

	query := `
		SELECT
			id,
			journal_id,
			label,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "lesson"
		WHERE id = $1
	`

	var (
		Id        sql.NullString
		JournalId sql.NullString
		Label     sql.NullString
		CreatedAt sql.NullString
		UpdatedAt sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&Id,
		&JournalId,
		&Label,
		&CreatedAt,
		&UpdatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &admin_service.Lesson{
		Id:        Id.String,
		JournalId: JournalId.String,
		Label:     Label.String,
		CreatedAt: CreatedAt.String,
		UpdatedAt: UpdatedAt.String,
	}

	return
}

func (c *lessonRepo) GetAll(ctx context.Context, req *admin_service.GetListLessonRequest) (resp *admin_service.GetListLessonResponse, err error) {

	resp = &admin_service.GetListLessonResponse{}

	var (
		query  string
		limit  = " LIMIT ALL"
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			journal_id,
			label,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "lesson"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	if len(req.Search) > 0 {
		query += " WHERE label ILIKE '" + req.Search + "' OR label ILIKE '%" + req.Search + "' OR label ILIKE '" + req.Search + "%'"
	} else {
		query += filter
	}
	query += sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	if err != nil {
		return resp, err
	}
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			Id        sql.NullString
			JournalId sql.NullString
			Label     sql.NullString
			CreatedAt sql.NullString
			UpdatedAt sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&Id,
			&JournalId,
			&Label,
			&CreatedAt,
			&UpdatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Lessons = append(resp.Lessons, &admin_service.Lesson{
			Id:        Id.String,
			JournalId: JournalId.String,
			Label:     Label.String,
			CreatedAt: CreatedAt.String,
			UpdatedAt: UpdatedAt.String,
		})
	}

	return
}

func (c *lessonRepo) Update(ctx context.Context, req *admin_service.UpdateLesson) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "lesson"
			SET
				id = :id,
				journal_id = :journal_id,
				label = :label,
				updated_at = NOW()
			WHERE
				id = :id`
	params = map[string]interface{}{
		"id":         req.GetId(),
		"journal_id": req.GetJournalId(),
		"label":      req.GetLabel(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *lessonRepo) Delete(ctx context.Context, req *admin_service.LessonPrimaryKey) error {

	query := `DELETE FROM "lesson" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return err
	}

	return nil
}
