package memory

import (
	"admin_service/genproto/admin_service"
	"admin_service/packages/helper"
	"admin_service/storage"
	"context"
	"database/sql"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type taskRepo struct {
	db *pgxpool.Pool
}

func NewTaskRepo(db *pgxpool.Pool) storage.TaskRepoI {
	return &taskRepo{
		db: db,
	}
}

func (c *taskRepo) Create(ctx context.Context, req *admin_service.CreateTask) (resp *admin_service.TaskPrimaryKey, err error) {

	var (
		id = uuid.New().String()
	)

	query := `
		INSERT INTO "task" (
			id,
			lesson_id,
			label,
			deadline,
			updated_at
			) VALUES
			($1, $2, $3, $4, NOW())
		`

	_, err = c.db.Exec(ctx,
		query,
		id,
		req.LessonId,
		req.Label,
		req.Deadline,
	)

	if err != nil {
		return nil, err
	}

	return &admin_service.TaskPrimaryKey{Id: id}, nil
}

func (c *taskRepo) GetByPKey(ctx context.Context, req *admin_service.TaskPrimaryKey) (resp *admin_service.Task, err error) {

	query := `
		SELECT
			id,
			lesson_id,
			label,
			deadline,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "task"
		WHERE id = $1
	`

	var (
		Id        sql.NullString
		LessonId  sql.NullString
		Label     sql.NullString
		Deadline  sql.NullString
		CreatedAt sql.NullString
		UpdatedAt sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&Id,
		&LessonId,
		&Label,
		&Deadline,
		&CreatedAt,
		&UpdatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &admin_service.Task{
		Id:        Id.String,
		LessonId:  LessonId.String,
		Label:     Label.String,
		Deadline:  Deadline.String,
		CreatedAt: CreatedAt.String,
		UpdatedAt: UpdatedAt.String,
	}

	return
}

func (c *taskRepo) GetAll(ctx context.Context, req *admin_service.GetListTaskRequest) (resp *admin_service.GetListTaskResponse, err error) {

	resp = &admin_service.GetListTaskResponse{}

	var (
		query  string
		limit  = " LIMIT ALL"
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			lesson_id,
			label,
			deadline,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "task"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	if len(req.Search) > 0 {
		query += " WHERE label ILIKE '" + req.Search + "' OR label ILIKE '%" + req.Search + "' OR label ILIKE '" + req.Search + "%'"
		query += " OR deadline ILIKE '" + req.Search + "' OR deadline ILIKE '%" + req.Search + "' OR deadline ILIKE '" + req.Search + "%'"
	} else {
		query += filter
	}
	query += sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	if err != nil {
		return resp, err
	}
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			Id        sql.NullString
			LessonId  sql.NullString
			Label     sql.NullString
			Deadline  sql.NullString
			CreatedAt sql.NullString
			UpdatedAt sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&Id,
			&LessonId,
			&Label,
			&Deadline,
			&CreatedAt,
			&UpdatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Tasks = append(resp.Tasks, &admin_service.Task{
			Id:        Id.String,
			LessonId:  LessonId.String,
			Label:     Label.String,
			Deadline:  Deadline.String,
			CreatedAt: CreatedAt.String,
			UpdatedAt: UpdatedAt.String,
		})
	}

	return
}

func (c *taskRepo) Update(ctx context.Context, req *admin_service.UpdateTask) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "task"
			SET
				id = :id,
				lesson_id = :lesson_id,
				label = :label,
				deadline = :deadline,
				updated_at = NOW()
			WHERE
				id = :id`
	params = map[string]interface{}{
		"id":        req.GetId(),
		"lesson_id": req.GetLessonId(),
		"label":     req.GetLabel(),
		"deadline":  req.GetDeadline(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *taskRepo) Delete(ctx context.Context, req *admin_service.TaskPrimaryKey) error {

	query := `DELETE FROM "task" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return err
	}

	return nil
}
