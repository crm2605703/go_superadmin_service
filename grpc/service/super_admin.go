package service

import (
	"admin_service/config"
	"admin_service/genproto/admin_service"
	"admin_service/grpc/client"
	"admin_service/packages/logger"
	"admin_service/storage"
	"context"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type SuperAdminService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*admin_service.UnimplementedSuperAdminServiceServer
}

func NewSuperAdminService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *SuperAdminService {
	return &SuperAdminService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *SuperAdminService) Create(ctx context.Context, req *admin_service.CreateSuperAdmin) (resp *admin_service.SuperAdmin, err error) {

	i.log.Info("---CreateSuperAdmin------>", logger.Any("req", req))

	pKey, err := i.strg.SuperAdmin().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateSuperAdmin->SuperAdmin->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.SuperAdmin().GetByPKey(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByPKeySuperAdmin->SuperAdmin->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *SuperAdminService) GetByID(ctx context.Context, req *admin_service.SuperAdminPrimaryKey) (resp *admin_service.SuperAdmin, err error) {

	i.log.Info("---GetSuperAdminByID------>", logger.Any("req", req))

	resp, err = i.strg.SuperAdmin().GetByPKey(ctx, req)
	if err != nil {
		i.log.Error("!!!GetSuperAdminByID->SuperAdmin->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *SuperAdminService) GetList(ctx context.Context, req *admin_service.GetListSuperAdminRequest) (resp *admin_service.GetListSuperAdminResponse, err error) {

	i.log.Info("---GetSuperAdmins------>", logger.Any("req", req))

	resp, err = i.strg.SuperAdmin().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetSuperAdmins->SuperAdmin->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *SuperAdminService) Update(ctx context.Context, req *admin_service.UpdateSuperAdmin) (resp *admin_service.SuperAdmin, err error) {

	i.log.Info("---UpdateSuperAdmin------>", logger.Any("req", req))

	rowsAffected, err := i.strg.SuperAdmin().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateSuperAdmin--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.SuperAdmin().GetByPKey(ctx, &admin_service.SuperAdminPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetSuperAdmin->SuperAdmin->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return
}

func (i *SuperAdminService) Delete(ctx context.Context, req *admin_service.SuperAdminPrimaryKey) (resp *admin_service.Empty9, err error) {

	i.log.Info("---DeleteSuperAdmin------>", logger.Any("req", req))

	err = i.strg.SuperAdmin().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteSuperAdmin->SuperAdmin->Get--->", logger.Error(err))
		return &admin_service.Empty9{}, status.Error(codes.InvalidArgument, err.Error())
	}

	return &admin_service.Empty9{}, nil
}
