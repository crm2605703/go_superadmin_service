package service

import (
	"admin_service/config"
	"admin_service/genproto/admin_service"
	"admin_service/grpc/client"
	"admin_service/packages/logger"
	"admin_service/storage"
	"context"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type ManagerService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*admin_service.UnimplementedManagerServiceServer
}

func NewManagerService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *ManagerService {
	return &ManagerService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *ManagerService) Create(ctx context.Context, req *admin_service.CreateManager) (resp *admin_service.Manager, err error) {

	i.log.Info("---CreateManager------>", logger.Any("req", req))

	pKey, err := i.strg.Manager().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateManager->Manager->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.Manager().GetByPKey(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByPKeyManager->Manager->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *ManagerService) GetByID(ctx context.Context, req *admin_service.ManagerPrimaryKey) (resp *admin_service.Manager, err error) {

	i.log.Info("---GetManagerByID------>", logger.Any("req", req))

	resp, err = i.strg.Manager().GetByPKey(ctx, req)
	if err != nil {
		i.log.Error("!!!GetManagerByID->Manager->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *ManagerService) GetList(ctx context.Context, req *admin_service.GetListManagerRequest) (resp *admin_service.GetListManagerResponse, err error) {

	i.log.Info("---GetManagers------>", logger.Any("req", req))

	resp, err = i.strg.Manager().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetManagers->Manager->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *ManagerService) Update(ctx context.Context, req *admin_service.UpdateManager) (resp *admin_service.Manager, err error) {

	i.log.Info("---UpdateManager------>", logger.Any("req", req))

	rowsAffected, err := i.strg.Manager().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateManager--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Manager().GetByPKey(ctx, &admin_service.ManagerPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetManager->Manager->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return
}

func (i *ManagerService) Delete(ctx context.Context, req *admin_service.ManagerPrimaryKey) (resp *admin_service.Empty6, err error) {

	i.log.Info("---DeleteManager------>", logger.Any("req", req))

	err = i.strg.Manager().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteManager->Manager->Get--->", logger.Error(err))
		return &admin_service.Empty6{}, status.Error(codes.InvalidArgument, err.Error())
	}

	return &admin_service.Empty6{}, nil
}
