package service

import (
	"admin_service/config"
	"admin_service/genproto/admin_service"
	"admin_service/grpc/client"
	"admin_service/packages/logger"
	"admin_service/storage"
	"context"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type EventService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*admin_service.UnimplementedEventServiceServer
}

func NewEventService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *EventService {
	return &EventService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *EventService) Create(ctx context.Context, req *admin_service.CreateEvent) (resp *admin_service.Event, err error) {

	i.log.Info("---CreateEvent------>", logger.Any("req", req))

	pKey, err := i.strg.Event().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateEvent->Event->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.Event().GetByPKey(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByPKeyEvent->Event->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *EventService) GetByID(ctx context.Context, req *admin_service.EventPrimaryKey) (resp *admin_service.Event, err error) {

	i.log.Info("---GetEventByID------>", logger.Any("req", req))

	resp, err = i.strg.Event().GetByPKey(ctx, req)
	if err != nil {
		i.log.Error("!!!GetEventByID->Event->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *EventService) GetList(ctx context.Context, req *admin_service.GetListEventRequest) (resp *admin_service.GetListEventResponse, err error) {

	i.log.Info("---GetEvents------>", logger.Any("req", req))

	resp, err = i.strg.Event().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetEvents->Event->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *EventService) Update(ctx context.Context, req *admin_service.UpdateEvent) (resp *admin_service.Event, err error) {

	i.log.Info("---UpdateEvent------>", logger.Any("req", req))

	rowsAffected, err := i.strg.Event().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateEvent--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Event().GetByPKey(ctx, &admin_service.EventPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetEvent->Event->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return
}

func (i *EventService) Delete(ctx context.Context, req *admin_service.EventPrimaryKey) (resp *admin_service.Empty3, err error) {

	i.log.Info("---DeleteEvent------>", logger.Any("req", req))

	err = i.strg.Event().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteEvent->Event->Get--->", logger.Error(err))
		return &admin_service.Empty3{}, status.Error(codes.InvalidArgument, err.Error())
	}

	return &admin_service.Empty3{}, nil
}
