package service

import (
	"admin_service/config"
	"admin_service/genproto/admin_service"
	"admin_service/grpc/client"
	"admin_service/packages/logger"
	"admin_service/storage"
	"context"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type JournalService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*admin_service.UnimplementedJournalServiceServer
}

func NewJournalService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *JournalService {
	return &JournalService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *JournalService) Create(ctx context.Context, req *admin_service.CreateJournal) (resp *admin_service.Journal, err error) {

	i.log.Info("---CreateJournal------>", logger.Any("req", req))

	pKey, err := i.strg.Journal().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateJournal->Journal->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.Journal().GetByPKey(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByPKeyJournal->Journal->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *JournalService) GetByID(ctx context.Context, req *admin_service.JournalPrimaryKey) (resp *admin_service.Journal, err error) {

	i.log.Info("---GetJournalByID------>", logger.Any("req", req))

	resp, err = i.strg.Journal().GetByPKey(ctx, req)
	if err != nil {
		i.log.Error("!!!GetJournalByID->Journal->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *JournalService) GetList(ctx context.Context, req *admin_service.GetListJournalRequest) (resp *admin_service.GetListJournalResponse, err error) {

	i.log.Info("---GetJournals------>", logger.Any("req", req))

	resp, err = i.strg.Journal().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetJournals->Journal->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *JournalService) Update(ctx context.Context, req *admin_service.UpdateJournal) (resp *admin_service.Journal, err error) {

	i.log.Info("---UpdateJournal------>", logger.Any("req", req))

	rowsAffected, err := i.strg.Journal().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateJournal--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Journal().GetByPKey(ctx, &admin_service.JournalPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetJournal->Journal->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return
}

func (i *JournalService) Delete(ctx context.Context, req *admin_service.JournalPrimaryKey) (resp *admin_service.Empty5, err error) {

	i.log.Info("---DeleteJournal------>", logger.Any("req", req))

	err = i.strg.Journal().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteJournal->Journal->Get--->", logger.Error(err))
		return &admin_service.Empty5{}, status.Error(codes.InvalidArgument, err.Error())
	}

	return &admin_service.Empty5{}, nil
}
