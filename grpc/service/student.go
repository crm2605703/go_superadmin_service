package service

import (
	"admin_service/config"
	"admin_service/genproto/admin_service"
	"admin_service/grpc/client"
	"admin_service/packages/logger"
	"admin_service/storage"
	"context"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type StudentService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*admin_service.UnimplementedStudentServiceServer
}

func NewStudentService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *StudentService {
	return &StudentService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *StudentService) Create(ctx context.Context, req *admin_service.CreateStudent) (resp *admin_service.Student, err error) {

	i.log.Info("---CreateStudent------>", logger.Any("req", req))

	pKey, err := i.strg.Student().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateStudent->Student->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.Student().GetByPKey(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByPKeyStudent->Student->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *StudentService) GetByID(ctx context.Context, req *admin_service.StudentPrimaryKey) (resp *admin_service.Student, err error) {

	i.log.Info("---GetStudentByID------>", logger.Any("req", req))

	resp, err = i.strg.Student().GetByPKey(ctx, req)
	if err != nil {
		i.log.Error("!!!GetStudentByID->Student->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *StudentService) GetList(ctx context.Context, req *admin_service.GetListStudentRequest) (resp *admin_service.GetListStudentResponse, err error) {

	i.log.Info("---GetStudents------>", logger.Any("req", req))

	resp, err = i.strg.Student().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetStudents->Student->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *StudentService) Update(ctx context.Context, req *admin_service.UpdateStudent) (resp *admin_service.Student, err error) {

	i.log.Info("---UpdateStudent------>", logger.Any("req", req))

	rowsAffected, err := i.strg.Student().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateStudent--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Student().GetByPKey(ctx, &admin_service.StudentPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetStudent->Student->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return
}

func (i *StudentService) Delete(ctx context.Context, req *admin_service.StudentPrimaryKey) (resp *admin_service.Empty18, err error) {

	i.log.Info("---DeleteStudent------>", logger.Any("req", req))

	err = i.strg.Student().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteStudent->Student->Get--->", logger.Error(err))
		return &admin_service.Empty18{}, status.Error(codes.InvalidArgument, err.Error())
	}

	return &admin_service.Empty18{}, nil
}
