package service

import (
	"admin_service/config"
	"admin_service/genproto/admin_service"
	"admin_service/grpc/client"
	"admin_service/packages/logger"
	"admin_service/storage"
	"context"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type UserBranchService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*admin_service.UnimplementedUserBranchServiceServer
}

func NewUserBranchService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *UserBranchService {
	return &UserBranchService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *UserBranchService) Create(ctx context.Context, req *admin_service.CreateUserBranch) (resp *admin_service.UserBranch, err error) {

	i.log.Info("---CreateUserBranch------>", logger.Any("req", req))

	pKey, err := i.strg.UserBranch().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateUserBranch->UserBranch->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.UserBranch().GetByPKey(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByPKeyUserBranch->UserBranch->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *UserBranchService) GetByID(ctx context.Context, req *admin_service.UserBranchPrimaryKey) (resp *admin_service.UserBranch, err error) {

	i.log.Info("---GetUserBranchByID------>", logger.Any("req", req))

	resp, err = i.strg.UserBranch().GetByPKey(ctx, req)
	if err != nil {
		i.log.Error("!!!GetUserBranchByID->UserBranch->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *UserBranchService) GetList(ctx context.Context, req *admin_service.GetListUserBranchRequest) (resp *admin_service.GetListUserBranchResponse, err error) {

	i.log.Info("---GetUserBranchs------>", logger.Any("req", req))

	resp, err = i.strg.UserBranch().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetUserBranchs->UserBranch->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *UserBranchService) Update(ctx context.Context, req *admin_service.UpdateUserBranch) (resp *admin_service.UserBranch, err error) {

	i.log.Info("---UpdateUserBranch------>", logger.Any("req", req))

	rowsAffected, err := i.strg.UserBranch().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateUserBranch--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.UserBranch().GetByPKey(ctx, &admin_service.UserBranchPrimaryKey{Login: req.Login, Password: req.Password})
	if err != nil {
		i.log.Error("!!!GetUserBranch->UserBranch->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return
}

func (i *UserBranchService) Delete(ctx context.Context, req *admin_service.UserBranchPrimaryKey) (resp *admin_service.Empty16, err error) {

	i.log.Info("---DeleteUserBranch------>", logger.Any("req", req))

	err = i.strg.UserBranch().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteUserBranch->UserBranch->Get--->", logger.Error(err))
		return &admin_service.Empty16{}, status.Error(codes.InvalidArgument, err.Error())
	}

	return &admin_service.Empty16{}, nil
}
