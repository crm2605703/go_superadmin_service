package service

import (
	"admin_service/config"
	"admin_service/genproto/admin_service"
	"admin_service/grpc/client"
	"admin_service/packages/logger"
	"admin_service/storage"
	"context"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type LessonService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*admin_service.UnimplementedLessonServiceServer
}

func NewLessonService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *LessonService {
	return &LessonService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *LessonService) Create(ctx context.Context, req *admin_service.CreateLesson) (resp *admin_service.Lesson, err error) {

	i.log.Info("---CreateLesson------>", logger.Any("req", req))

	pKey, err := i.strg.Lesson().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateLesson->Lesson->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.Lesson().GetByPKey(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByPKeyLesson->Lesson->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *LessonService) GetByID(ctx context.Context, req *admin_service.LessonPrimaryKey) (resp *admin_service.Lesson, err error) {

	i.log.Info("---GetLessonByID------>", logger.Any("req", req))

	resp, err = i.strg.Lesson().GetByPKey(ctx, req)
	if err != nil {
		i.log.Error("!!!GetLessonByID->Lesson->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *LessonService) GetList(ctx context.Context, req *admin_service.GetListLessonRequest) (resp *admin_service.GetListLessonResponse, err error) {

	i.log.Info("---GetLessons------>", logger.Any("req", req))

	resp, err = i.strg.Lesson().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetLessons->Lesson->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *LessonService) Update(ctx context.Context, req *admin_service.UpdateLesson) (resp *admin_service.Lesson, err error) {

	i.log.Info("---UpdateLesson------>", logger.Any("req", req))

	rowsAffected, err := i.strg.Lesson().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateLesson--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Lesson().GetByPKey(ctx, &admin_service.LessonPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetLesson->Lesson->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return
}

func (i *LessonService) Delete(ctx context.Context, req *admin_service.LessonPrimaryKey) (resp *admin_service.Empty13, err error) {

	i.log.Info("---DeleteLesson------>", logger.Any("req", req))

	err = i.strg.Lesson().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteLesson->Lesson->Get--->", logger.Error(err))
		return &admin_service.Empty13{}, status.Error(codes.InvalidArgument, err.Error())
	}

	return &admin_service.Empty13{}, nil
}
