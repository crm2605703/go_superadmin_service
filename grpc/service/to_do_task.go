package service

import (
	"admin_service/config"
	"admin_service/genproto/admin_service"
	"admin_service/grpc/client"
	"admin_service/packages/logger"
	"admin_service/storage"
	"context"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type ToDoTaskService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*admin_service.UnimplementedToDoTaskServiceServer
}

func NewToDoTaskService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *ToDoTaskService {
	return &ToDoTaskService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *ToDoTaskService) Create(ctx context.Context, req *admin_service.CreateToDoTask) (resp *admin_service.ToDoTask, err error) {

	i.log.Info("---CreateToDoTask------>", logger.Any("req", req))

	pKey, err := i.strg.ToDoTask().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateToDoTask->ToDoTask->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.ToDoTask().GetByPKey(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByPKeyToDoTask->ToDoTask->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *ToDoTaskService) GetByID(ctx context.Context, req *admin_service.ToDoTaskPrimaryKey) (resp *admin_service.ToDoTask, err error) {

	i.log.Info("---GetToDoTaskByID------>", logger.Any("req", req))

	resp, err = i.strg.ToDoTask().GetByPKey(ctx, req)
	if err != nil {
		i.log.Error("!!!GetToDoTaskByID->ToDoTask->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *ToDoTaskService) GetList(ctx context.Context, req *admin_service.GetListToDoTaskRequest) (resp *admin_service.GetListToDoTaskResponse, err error) {

	i.log.Info("---GetToDoTasks------>", logger.Any("req", req))

	resp, err = i.strg.ToDoTask().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetToDoTasks->ToDoTask->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *ToDoTaskService) Update(ctx context.Context, req *admin_service.UpdateToDoTask) (resp *admin_service.ToDoTask, err error) {

	i.log.Info("---UpdateToDoTask------>", logger.Any("req", req))

	rowsAffected, err := i.strg.ToDoTask().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateToDoTask--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.ToDoTask().GetByPKey(ctx, &admin_service.ToDoTaskPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetToDoTask->ToDoTask->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return
}

func (i *ToDoTaskService) Delete(ctx context.Context, req *admin_service.ToDoTaskPrimaryKey) (resp *admin_service.Empty14, err error) {

	i.log.Info("---DeleteToDoTask------>", logger.Any("req", req))

	err = i.strg.ToDoTask().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteToDoTask->ToDoTask->Get--->", logger.Error(err))
		return &admin_service.Empty14{}, status.Error(codes.InvalidArgument, err.Error())
	}

	return &admin_service.Empty14{}, nil
}
