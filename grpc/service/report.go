package service

import (
	config "admin_service/config"
	"admin_service/genproto/admin_service"
	"admin_service/grpc/client"
	"admin_service/packages/logger"
	storage "admin_service/storage"
	"context"
	"fmt"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type ReportService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*admin_service.UnimplementedReportServiceServer
}

func NewReportService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *ReportService {
	return &ReportService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *ReportService) GetTeacher(ctx context.Context, req *admin_service.ReportRequest) (resp *admin_service.TeacherReportList, err error) {

	i.log.Info("---Get Teacher Report------>", logger.Any("req", req))

	resp, err = i.strg.Report().GetTeacher(ctx, req)
	if err != nil {
		i.log.Error("!!!GetTeacher->Report->GET--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	fmt.Println(resp)

	return
}

func (i *ReportService) GetStudent(ctx context.Context, req *admin_service.ReportRequest) (resp *admin_service.StudentList, err error) {

	i.log.Info("---Get Student Report------>", logger.Any("req", req))

	resp, err = i.strg.Report().GetStudent(ctx, req)
	if err != nil {
		i.log.Error("!!!GetStudent->Report->GET--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	fmt.Println(resp)

	return
}

func (i *ReportService) GetAdministrator(ctx context.Context, req *admin_service.ReportRequest) (resp *admin_service.AdminList, err error) {

	i.log.Info("---Get Admin Report------>", logger.Any("req", req))

	resp, err = i.strg.Report().GetAdministrator(ctx, req)
	if err != nil {
		fmt.Println("JJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJ")
		i.log.Error("!!!GetAdmin->Report->GET--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	fmt.Println(resp)

	return
}

func (i *ReportService) GetSupportTeacher(ctx context.Context, req *admin_service.ReportRequest) (resp *admin_service.SupportTeacherList, err error) {

	i.log.Info("---Get SupportTeacher Report------>", logger.Any("req", req))

	resp, err = i.strg.Report().GetSupportTeacher(ctx, req)
	if err != nil {
		i.log.Error("!!!GetSupportTeacher->Report->GET--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	fmt.Println(resp)

	return
}

func (i *ReportService) TeacherReport(ctx context.Context, req *admin_service.TeacherId) (resp *admin_service.TeacherArrList, err error) {

	i.log.Info("---Get Teacher Report------>", logger.Any("req", req))

	resp, err = i.strg.Report().TeacherReport(ctx, req)
	if err != nil {
		i.log.Error("!!!GetTeacher->Report->GET--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	fmt.Println(resp)

	return
}

func (i *ReportService) SupportTeacherReport(ctx context.Context, req *admin_service.SupportTeacherId) (resp *admin_service.SupportArrList, err error) {

	i.log.Info("---Get Teacher Report------>", logger.Any("req", req))

	resp, err = i.strg.Report().SupportTeacherReport(ctx, req)
	if err != nil {
		i.log.Error("!!!GetTeacher->Report->GET--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	fmt.Println(resp)

	return
}

func (i *ReportService) StudentReport(ctx context.Context, req *admin_service.StudentId) (resp *admin_service.StudentArrList, err error) {

	i.log.Info("---Get Student Report------>", logger.Any("req", req))

	resp, err = i.strg.Report().StudentReport(ctx, req)
	if err != nil {
		i.log.Error("!!!GetStudent->Report->GET--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	fmt.Println(resp)

	return
}
