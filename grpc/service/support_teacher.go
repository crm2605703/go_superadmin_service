package service

import (
	"admin_service/config"
	"admin_service/genproto/admin_service"
	"admin_service/grpc/client"
	"admin_service/packages/logger"
	"admin_service/storage"
	"context"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type SupportTeacherService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*admin_service.UnimplementedSupportTeacherServiceServer
}

func NewSupportTeacherService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *SupportTeacherService {
	return &SupportTeacherService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *SupportTeacherService) Create(ctx context.Context, req *admin_service.CreateSupportTeacher) (resp *admin_service.SupportTeacher, err error) {

	i.log.Info("---CreateSupportTeacher------>", logger.Any("req", req))

	pKey, err := i.strg.SupportTeacher().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateSupportTeacher->SupportTeacher->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.SupportTeacher().GetByPKey(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByPKeySupportTeacher->SupportTeacher->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *SupportTeacherService) GetByID(ctx context.Context, req *admin_service.SupportTeacherPrimaryKey) (resp *admin_service.SupportTeacher, err error) {

	i.log.Info("---GetSupportTeacherByID------>", logger.Any("req", req))

	resp, err = i.strg.SupportTeacher().GetByPKey(ctx, req)
	if err != nil {
		i.log.Error("!!!GetSupportTeacherByID->SupportTeacher->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *SupportTeacherService) GetList(ctx context.Context, req *admin_service.GetListSupportTeacherRequest) (resp *admin_service.GetListSupportTeacherResponse, err error) {

	i.log.Info("---GetSupportTeachers------>", logger.Any("req", req))

	resp, err = i.strg.SupportTeacher().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetSupportTeachers->SupportTeacher->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *SupportTeacherService) Update(ctx context.Context, req *admin_service.UpdateSupportTeacher) (resp *admin_service.SupportTeacher, err error) {

	i.log.Info("---UpdateSupportTeacher------>", logger.Any("req", req))

	rowsAffected, err := i.strg.SupportTeacher().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateSupportTeacher--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.SupportTeacher().GetByPKey(ctx, &admin_service.SupportTeacherPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetSupportTeacher->SupportTeacher->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return
}

func (i *SupportTeacherService) Delete(ctx context.Context, req *admin_service.SupportTeacherPrimaryKey) (resp *admin_service.Empty10, err error) {

	i.log.Info("---DeleteSupportTeacher------>", logger.Any("req", req))

	err = i.strg.SupportTeacher().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteSupportTeacher->SupportTeacher->Get--->", logger.Error(err))
		return &admin_service.Empty10{}, status.Error(codes.InvalidArgument, err.Error())
	}

	return &admin_service.Empty10{}, nil
}
