package service

import (
	"admin_service/config"
	"admin_service/genproto/admin_service"
	"admin_service/grpc/client"
	"admin_service/packages/logger"
	"admin_service/storage"
	"context"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type ScheduleWeekService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*admin_service.UnimplementedScheduleWeekServiceServer
}

func NewScheduleWeekService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *ScheduleWeekService {
	return &ScheduleWeekService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *ScheduleWeekService) Create(ctx context.Context, req *admin_service.CreateScheduleWeek) (resp *admin_service.ScheduleWeek, err error) {

	i.log.Info("---CreateScheduleWeek------>", logger.Any("req", req))

	pKey, err := i.strg.ScheduleWeek().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateScheduleWeek->ScheduleWeek->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.ScheduleWeek().GetByPKey(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByPKeyScheduleWeek->ScheduleWeek->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *ScheduleWeekService) GetByID(ctx context.Context, req *admin_service.ScheduleWeekPrimaryKey) (resp *admin_service.ScheduleWeek, err error) {

	i.log.Info("---GetScheduleWeekByID------>", logger.Any("req", req))

	resp, err = i.strg.ScheduleWeek().GetByPKey(ctx, req)
	if err != nil {
		i.log.Error("!!!GetScheduleWeekByID->ScheduleWeek->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *ScheduleWeekService) GetList(ctx context.Context, req *admin_service.GetListScheduleWeekRequest) (resp *admin_service.GetListScheduleWeekResponse, err error) {

	i.log.Info("---GetScheduleWeeks------>", logger.Any("req", req))

	resp, err = i.strg.ScheduleWeek().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetScheduleWeeks->ScheduleWeek->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *ScheduleWeekService) Update(ctx context.Context, req *admin_service.UpdateScheduleWeek) (resp *admin_service.ScheduleWeek, err error) {

	i.log.Info("---UpdateScheduleWeek------>", logger.Any("req", req))

	rowsAffected, err := i.strg.ScheduleWeek().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateScheduleWeek--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.ScheduleWeek().GetByPKey(ctx, &admin_service.ScheduleWeekPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetScheduleWeek->ScheduleWeek->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return
}

func (i *ScheduleWeekService) Delete(ctx context.Context, req *admin_service.ScheduleWeekPrimaryKey) (resp *admin_service.Empty7, err error) {

	i.log.Info("---DeleteScheduleWeek------>", logger.Any("req", req))

	err = i.strg.ScheduleWeek().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteScheduleWeek->ScheduleWeek->Get--->", logger.Error(err))
		return &admin_service.Empty7{}, status.Error(codes.InvalidArgument, err.Error())
	}

	return &admin_service.Empty7{}, nil
}
