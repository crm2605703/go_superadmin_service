package service

import (
	"admin_service/config"
	"admin_service/genproto/admin_service"
	"admin_service/grpc/client"
	"admin_service/packages/logger"
	"admin_service/storage"
	"context"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type GroupService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*admin_service.UnimplementedGroupServiceServer
}

func NewGroupService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *GroupService {
	return &GroupService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *GroupService) Create(ctx context.Context, req *admin_service.CreateGroup) (resp *admin_service.Group, err error) {

	i.log.Info("---CreateGroup------>", logger.Any("req", req))

	pKey, err := i.strg.Group().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateGroup->Group->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.Group().GetByPKey(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByPKeyGroup->Group->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *GroupService) GetByID(ctx context.Context, req *admin_service.GroupPrimaryKey) (resp *admin_service.Group, err error) {

	i.log.Info("---GetGroupByID------>", logger.Any("req", req))

	resp, err = i.strg.Group().GetByPKey(ctx, req)
	if err != nil {
		i.log.Error("!!!GetGroupByID->Group->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *GroupService) GetList(ctx context.Context, req *admin_service.GetListGroupRequest) (resp *admin_service.GetListGroupResponse, err error) {

	i.log.Info("---GetGroups------>", logger.Any("req", req))

	resp, err = i.strg.Group().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetGroups->Group->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *GroupService) Update(ctx context.Context, req *admin_service.UpdateGroup) (resp *admin_service.Group, err error) {

	i.log.Info("---UpdateGroup------>", logger.Any("req", req))

	rowsAffected, err := i.strg.Group().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateGroup--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Group().GetByPKey(ctx, &admin_service.GroupPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetGroup->Group->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return
}

func (i *GroupService) Delete(ctx context.Context, req *admin_service.GroupPrimaryKey) (resp *admin_service.Empty4, err error) {

	i.log.Info("---DeleteGroup------>", logger.Any("req", req))

	err = i.strg.Group().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteGroup->Group->Get--->", logger.Error(err))
		return &admin_service.Empty4{}, status.Error(codes.InvalidArgument, err.Error())
	}

	return &admin_service.Empty4{}, nil
}
