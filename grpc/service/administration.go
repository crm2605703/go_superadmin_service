package service

import (
	"admin_service/config"
	"admin_service/genproto/admin_service"

	"admin_service/grpc/client"
	"admin_service/packages/logger"
	"admin_service/storage"
	"context"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type AdministrationService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*admin_service.UnimplementedAdministrationServiceServer
}

func NewAdministrationService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *AdministrationService {
	return &AdministrationService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *AdministrationService) Create(ctx context.Context, req *admin_service.CreateAdministration) (resp *admin_service.Administration, err error) {

	i.log.Info("---CreateAdministration------>", logger.Any("req", req))

	pKey, err := i.strg.Administration().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateAdministration->Administration->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.Administration().GetByPKey(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByPKeyAdministration->Administration->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *AdministrationService) GetByID(ctx context.Context, req *admin_service.AdministrationPrimaryKey) (resp *admin_service.Administration, err error) {

	i.log.Info("---GetAdministrationByID------>", logger.Any("req", req))

	resp, err = i.strg.Administration().GetByPKey(ctx, req)
	if err != nil {
		i.log.Error("!!!GetAdministrationByID->Administration->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *AdministrationService) GetList(ctx context.Context, req *admin_service.GetListAdministrationRequest) (resp *admin_service.GetListAdministrationResponse, err error) {

	i.log.Info("---GetAdministrations------>", logger.Any("req", req))

	resp, err = i.strg.Administration().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetAdministrations->Administration->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *AdministrationService) Update(ctx context.Context, req *admin_service.UpdateAdministration) (resp *admin_service.Administration, err error) {

	i.log.Info("---UpdateAdministration------>", logger.Any("req", req))

	rowsAffected, err := i.strg.Administration().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateAdministration--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Administration().GetByPKey(ctx, &admin_service.AdministrationPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetAdministration->Administration->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return
}

func (i *AdministrationService) Delete(ctx context.Context, req *admin_service.AdministrationPrimaryKey) (resp *admin_service.Empty1, err error) {

	i.log.Info("---DeleteAdministration------>", logger.Any("req", req))

	err = i.strg.Administration().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteAdministration->Administration->Get--->", logger.Error(err))
		return &admin_service.Empty1{}, status.Error(codes.InvalidArgument, err.Error())
	}

	return &admin_service.Empty1{}, nil
}
