package service

import (
	"admin_service/config"
	"admin_service/genproto/admin_service"
	"admin_service/grpc/client"
	"admin_service/packages/logger"
	"admin_service/storage"
	"context"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type TaskService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*admin_service.UnimplementedTaskServiceServer
}

func NewTaskService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *TaskService {
	return &TaskService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *TaskService) Create(ctx context.Context, req *admin_service.CreateTask) (resp *admin_service.Task, err error) {

	i.log.Info("---CreateTask------>", logger.Any("req", req))

	pKey, err := i.strg.Task().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateTask->Task->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.Task().GetByPKey(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByPKeyTask->Task->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *TaskService) GetByID(ctx context.Context, req *admin_service.TaskPrimaryKey) (resp *admin_service.Task, err error) {

	i.log.Info("---GetTaskByID------>", logger.Any("req", req))

	resp, err = i.strg.Task().GetByPKey(ctx, req)
	if err != nil {
		i.log.Error("!!!GetTaskByID->Task->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *TaskService) GetList(ctx context.Context, req *admin_service.GetListTaskRequest) (resp *admin_service.GetListTaskResponse, err error) {

	i.log.Info("---GetTasks------>", logger.Any("req", req))

	resp, err = i.strg.Task().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetTasks->Task->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *TaskService) Update(ctx context.Context, req *admin_service.UpdateTask) (resp *admin_service.Task, err error) {

	i.log.Info("---UpdateTask------>", logger.Any("req", req))

	rowsAffected, err := i.strg.Task().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateTask--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Task().GetByPKey(ctx, &admin_service.TaskPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetTask->Task->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return
}

func (i *TaskService) Delete(ctx context.Context, req *admin_service.TaskPrimaryKey) (resp *admin_service.Empty11, err error) {

	i.log.Info("---DeleteTask------>", logger.Any("req", req))

	err = i.strg.Task().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteTask->Task->Get--->", logger.Error(err))
		return &admin_service.Empty11{}, status.Error(codes.InvalidArgument, err.Error())
	}

	return &admin_service.Empty11{}, nil
}
