package grpc

import (
	"admin_service/config"
	"admin_service/genproto/admin_service"
	"admin_service/grpc/client"
	"admin_service/grpc/service"
	"admin_service/packages/logger"
	"admin_service/storage"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func SetUpServer(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) (grpcServer *grpc.Server) {

	grpcServer = grpc.NewServer()

	admin_service.RegisterAdministrationServiceServer(grpcServer, service.NewAdministrationService(cfg, log, strg, srvc))
	admin_service.RegisterAssignStudentServiceServer(grpcServer, service.NewAssignStudentService(cfg, log, strg, srvc))
	admin_service.RegisterBranchServiceServer(grpcServer, service.NewBranchService(cfg, log, strg, srvc))
	admin_service.RegisterEventServiceServer(grpcServer, service.NewEventService(cfg, log, strg, srvc))
	admin_service.RegisterGroupServiceServer(grpcServer, service.NewGroupService(cfg, log, strg, srvc))
	admin_service.RegisterJournalServiceServer(grpcServer, service.NewJournalService(cfg, log, strg, srvc))
	admin_service.RegisterLessonServiceServer(grpcServer, service.NewLessonService(cfg, log, strg, srvc))
	admin_service.RegisterManagerServiceServer(grpcServer, service.NewManagerService(cfg, log, strg, srvc))
	admin_service.RegisterScheduleWeekServiceServer(grpcServer, service.NewScheduleWeekService(cfg, log, strg, srvc))
	admin_service.RegisterStudentServiceServer(grpcServer, service.NewStudentService(cfg, log, strg, srvc))
	admin_service.RegisterSuperAdminServiceServer(grpcServer, service.NewSuperAdminService(cfg, log, strg, srvc))
	admin_service.RegisterSupportTeacherServiceServer(grpcServer, service.NewSupportTeacherService(cfg, log, strg, srvc))
	admin_service.RegisterTaskServiceServer(grpcServer, service.NewTaskService(cfg, log, strg, srvc))
	admin_service.RegisterTeacherServiceServer(grpcServer, service.NewTeacherService(cfg, log, strg, srvc))
	admin_service.RegisterToDoTaskServiceServer(grpcServer, service.NewToDoTaskService(cfg, log, strg, srvc))
	admin_service.RegisterUserBranchServiceServer(grpcServer, service.NewUserBranchService(cfg, log, strg, srvc))
	admin_service.RegisterReportServiceServer(grpcServer, service.NewReportService(cfg, log, strg, srvc))
	reflection.Register(grpcServer)
	return
}
