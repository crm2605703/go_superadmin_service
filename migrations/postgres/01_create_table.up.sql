CREATE TABLE "branch" (
  "id" UUID NOT NULL PRIMARY KEY,
  "title" VARCHAR,
  "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  "updated_at" TIMESTAMP
);

CREATE TABLE "super_admin" (
  "id" UUID NOT NULL PRIMARY KEY,
  "increment_id" VARCHAR,
  "branch_id" UUID REFERENCES "branch" ("id"),
  "name" VARCHAR,
  "surname" VARCHAR,
  "login" VARCHAR,
  "password" VARCHAR,
  "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  "updated_at" TIMESTAMP
);

CREATE TABLE "manager" (
  "id" UUID NOT NULL PRIMARY KEY,
  "increment_id" VARCHAR,
  "name" VARCHAR,
  "surname" VARCHAR,
  "phone" VARCHAR,
  "login" VARCHAR,
  "password" VARCHAR,
  "salary" NUMERIC,
  "branch_id" UUID REFERENCES "branch" ("id"),
  "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  "updated_at" TIMESTAMP
);

CREATE TABLE "teacher" (
  "id" UUID NOT NULL PRIMARY KEY,
  "increment_id" VARCHAR,
  "branch_id" UUID REFERENCES "branch" ("id"),
  "login" VARCHAR,
  "password" VARCHAR,
  "name" VARCHAR,
  "surname" VARCHAR,
  "phone" VARCHAR,
  "salary" NUMERIC,
  "month_worked" INT,
  "attempts" INT,
  "total_sum" NUMERIC,
  "ielts_score" NUMERIC,
  "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  "updated_at" TIMESTAMP
);

CREATE TABLE "support_teacher" (
  "id" UUID NOT NULL PRIMARY KEY,
  "increment_id" VARCHAR,
  "branch_id" UUID REFERENCES "branch" ("id"),
  "login" VARCHAR,
  "password" VARCHAR,
  "name" VARCHAR,
  "surname" VARCHAR,
  "phone" VARCHAR,
  "salary" NUMERIC,
  "month_worked" INT,
  "total_sum" NUMERIC,
  "ielts_score" NUMERIC,
  "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  "updated_at" TIMESTAMP
);

CREATE TABLE "administration" (
  "id" UUID NOT NULL PRIMARY KEY,
  "increment_id" VARCHAR,
  "branch_id" UUID REFERENCES "branch" ("id"),
  "login" VARCHAR,
  "password" VARCHAR,
  "name" VARCHAR,
  "surname" VARCHAR,
  "phone" VARCHAR,
  "salary" NUMERIC,
  "month_worked" INT,
  "total_sum" NUMERIC,
  "ielts_score" NUMERIC,
  "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  "updated_at" TIMESTAMP
);

CREATE TABLE "group" (
  "id" UUID NOT NULL PRIMARY KEY,
  "group_type" VARCHAR CHECK ("group_type" IN ('Beginner', 'Elementary', 'Intermediate', 'Ielts')),
  "teacher_id" UUID REFERENCES "teacher" ("id"),
  "branch_id" UUID REFERENCES "branch" ("id"),
  "support_teacher_id" UUID REFERENCES "support_teacher" ("id"),
  "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  "updated_at" TIMESTAMP
);

CREATE TABLE "schedule_week" (
  "id" UUID NOT NULL PRIMARY KEY,
  "group_id" UUID REFERENCES "group" ("id"),
  "group_type" VARCHAR,
  "number_of_students" INT,
  "start_time" TIME,
  "end_time" TIME,
  "branch_id" UUID REFERENCES "branch" ("id"),
  "teacher_id" UUID REFERENCES "teacher" ("id"),
  "support_teacher_id" UUID REFERENCES "support_teacher" ("id"),
  "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  "updated_at" TIMESTAMP
);

CREATE TABLE "journal" (
  "id" UUID NOT NULL PRIMARY KEY,
  "schedules_id" UUID REFERENCES "schedule_week" ("id"),
  "group_id" UUID REFERENCES "group" ("id"),
  "from" DATE,
  "to" DATE,
  "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  "updated_at" TIMESTAMP
);

CREATE TABLE "student" (
  "id" UUID NOT NULL PRIMARY KEY,
  "increment_id" VARCHAR,
  "branch_id" UUID REFERENCES "branch" ("id"),
  "group_id" UUID REFERENCES "group" ("id"),
  "login" VARCHAR,
  "password" VARCHAR,
  "name" VARCHAR,
  "surname" VARCHAR,
  "phone" VARCHAR,
  "paid_sum" NUMERIC,
  "teacher_id" UUID REFERENCES "teacher" ("id"),
  "course_count" INT,
  "journal_id" UUID REFERENCES "journal" ("id"),
  "total_sum" NUMERIC,
  "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  "updated_at" TIMESTAMP
);

CREATE TABLE "lesson" (
  "id" UUID NOT NULL PRIMARY KEY,
  "journal_id" UUID REFERENCES "journal" ("id"),
  "label" VARCHAR,
  "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  "updated_at" TIMESTAMP
);

CREATE TABLE "task" (
  "id" UUID NOT NULL PRIMARY KEY,
  "lesson_id" UUID REFERENCES "lesson" ("id"),
  "label" VARCHAR,
  "deadline" DATE,
  "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  "updated_at" TIMESTAMP
);

CREATE TABLE "todo_task" (
  "id" UUID NOT NULL PRIMARY KEY,
  "student_id" UUID REFERENCES "student" ("id"),
  "task_id" UUID REFERENCES "task" ("id"),
  "score" INT,
  "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  "updated_at" TIMESTAMP
);

CREATE TABLE "event" (
  "id" UUID NOT NULL PRIMARY KEY,
  "branch_id" UUID REFERENCES "branch" ("id"),
  "topic" VARCHAR,
  "start_time" TIME,
  "day" DATE,
  "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  "updated_at" TIMESTAMP
);

CREATE TABLE "assign_student" (
  "id" UUID NOT NULL PRIMARY KEY,
  "branch_id" UUID REFERENCES "branch" ("id"),
  "student_id" UUID REFERENCES "student" ("id"),
  "event_id" UUID REFERENCES "event" ("id"),
  "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  "updated_at" TIMESTAMP
);

CREATE TABLE "user_branch" (
  "id" UUID NOT NULL PRIMARY KEY,
  "branch_id" UUID REFERENCES "branch" ("id"),
  "login" VARCHAR UNIQUE,
  "password" VARCHAR,
  "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  "updated_at" TIMESTAMP
);

CREATE TABLE "payment" (
  "id" UUID NOT NULL PRIMARY KEY,
  "student_id" UUID REFERENCES "student" ("id"),
  "branch_id" UUID REFERENCES "branch" ("id"),
  "paid_sum" NUMERIC,
  "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  "updated_at" TIMESTAMP
);
