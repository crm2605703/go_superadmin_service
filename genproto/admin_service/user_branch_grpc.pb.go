// Code generated by protoc-gen-go-grpc. DO NOT EDIT.
// versions:
// - protoc-gen-go-grpc v1.2.0
// - protoc             v3.21.12
// source: user_branch.proto

package admin_service

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

// UserBranchServiceClient is the client API for UserBranchService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type UserBranchServiceClient interface {
	Create(ctx context.Context, in *CreateUserBranch, opts ...grpc.CallOption) (*UserBranch, error)
	GetByID(ctx context.Context, in *UserBranchPrimaryKey, opts ...grpc.CallOption) (*UserBranch, error)
	GetList(ctx context.Context, in *GetListUserBranchRequest, opts ...grpc.CallOption) (*GetListUserBranchResponse, error)
	Update(ctx context.Context, in *UpdateUserBranch, opts ...grpc.CallOption) (*UserBranch, error)
	Delete(ctx context.Context, in *UserBranchPrimaryKey, opts ...grpc.CallOption) (*Empty16, error)
}

type userBranchServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewUserBranchServiceClient(cc grpc.ClientConnInterface) UserBranchServiceClient {
	return &userBranchServiceClient{cc}
}

func (c *userBranchServiceClient) Create(ctx context.Context, in *CreateUserBranch, opts ...grpc.CallOption) (*UserBranch, error) {
	out := new(UserBranch)
	err := c.cc.Invoke(ctx, "/admin_service.UserBranchService/Create", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *userBranchServiceClient) GetByID(ctx context.Context, in *UserBranchPrimaryKey, opts ...grpc.CallOption) (*UserBranch, error) {
	out := new(UserBranch)
	err := c.cc.Invoke(ctx, "/admin_service.UserBranchService/GetByID", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *userBranchServiceClient) GetList(ctx context.Context, in *GetListUserBranchRequest, opts ...grpc.CallOption) (*GetListUserBranchResponse, error) {
	out := new(GetListUserBranchResponse)
	err := c.cc.Invoke(ctx, "/admin_service.UserBranchService/GetList", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *userBranchServiceClient) Update(ctx context.Context, in *UpdateUserBranch, opts ...grpc.CallOption) (*UserBranch, error) {
	out := new(UserBranch)
	err := c.cc.Invoke(ctx, "/admin_service.UserBranchService/Update", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *userBranchServiceClient) Delete(ctx context.Context, in *UserBranchPrimaryKey, opts ...grpc.CallOption) (*Empty16, error) {
	out := new(Empty16)
	err := c.cc.Invoke(ctx, "/admin_service.UserBranchService/Delete", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// UserBranchServiceServer is the server API for UserBranchService service.
// All implementations should embed UnimplementedUserBranchServiceServer
// for forward compatibility
type UserBranchServiceServer interface {
	Create(context.Context, *CreateUserBranch) (*UserBranch, error)
	GetByID(context.Context, *UserBranchPrimaryKey) (*UserBranch, error)
	GetList(context.Context, *GetListUserBranchRequest) (*GetListUserBranchResponse, error)
	Update(context.Context, *UpdateUserBranch) (*UserBranch, error)
	Delete(context.Context, *UserBranchPrimaryKey) (*Empty16, error)
}

// UnimplementedUserBranchServiceServer should be embedded to have forward compatible implementations.
type UnimplementedUserBranchServiceServer struct {
}

func (UnimplementedUserBranchServiceServer) Create(context.Context, *CreateUserBranch) (*UserBranch, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Create not implemented")
}
func (UnimplementedUserBranchServiceServer) GetByID(context.Context, *UserBranchPrimaryKey) (*UserBranch, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetByID not implemented")
}
func (UnimplementedUserBranchServiceServer) GetList(context.Context, *GetListUserBranchRequest) (*GetListUserBranchResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetList not implemented")
}
func (UnimplementedUserBranchServiceServer) Update(context.Context, *UpdateUserBranch) (*UserBranch, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Update not implemented")
}
func (UnimplementedUserBranchServiceServer) Delete(context.Context, *UserBranchPrimaryKey) (*Empty16, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Delete not implemented")
}

// UnsafeUserBranchServiceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to UserBranchServiceServer will
// result in compilation errors.
type UnsafeUserBranchServiceServer interface {
	mustEmbedUnimplementedUserBranchServiceServer()
}

func RegisterUserBranchServiceServer(s grpc.ServiceRegistrar, srv UserBranchServiceServer) {
	s.RegisterService(&UserBranchService_ServiceDesc, srv)
}

func _UserBranchService_Create_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CreateUserBranch)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(UserBranchServiceServer).Create(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/admin_service.UserBranchService/Create",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(UserBranchServiceServer).Create(ctx, req.(*CreateUserBranch))
	}
	return interceptor(ctx, in, info, handler)
}

func _UserBranchService_GetByID_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(UserBranchPrimaryKey)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(UserBranchServiceServer).GetByID(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/admin_service.UserBranchService/GetByID",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(UserBranchServiceServer).GetByID(ctx, req.(*UserBranchPrimaryKey))
	}
	return interceptor(ctx, in, info, handler)
}

func _UserBranchService_GetList_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetListUserBranchRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(UserBranchServiceServer).GetList(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/admin_service.UserBranchService/GetList",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(UserBranchServiceServer).GetList(ctx, req.(*GetListUserBranchRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _UserBranchService_Update_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(UpdateUserBranch)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(UserBranchServiceServer).Update(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/admin_service.UserBranchService/Update",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(UserBranchServiceServer).Update(ctx, req.(*UpdateUserBranch))
	}
	return interceptor(ctx, in, info, handler)
}

func _UserBranchService_Delete_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(UserBranchPrimaryKey)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(UserBranchServiceServer).Delete(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/admin_service.UserBranchService/Delete",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(UserBranchServiceServer).Delete(ctx, req.(*UserBranchPrimaryKey))
	}
	return interceptor(ctx, in, info, handler)
}

// UserBranchService_ServiceDesc is the grpc.ServiceDesc for UserBranchService service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var UserBranchService_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "admin_service.UserBranchService",
	HandlerType: (*UserBranchServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "Create",
			Handler:    _UserBranchService_Create_Handler,
		},
		{
			MethodName: "GetByID",
			Handler:    _UserBranchService_GetByID_Handler,
		},
		{
			MethodName: "GetList",
			Handler:    _UserBranchService_GetList_Handler,
		},
		{
			MethodName: "Update",
			Handler:    _UserBranchService_Update_Handler,
		},
		{
			MethodName: "Delete",
			Handler:    _UserBranchService_Delete_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "user_branch.proto",
}
