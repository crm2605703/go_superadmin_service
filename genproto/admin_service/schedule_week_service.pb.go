// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.28.1
// 	protoc        v3.21.12
// source: schedule_week_service.proto

package admin_service

import (
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type Empty7 struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields
}

func (x *Empty7) Reset() {
	*x = Empty7{}
	if protoimpl.UnsafeEnabled {
		mi := &file_schedule_week_service_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Empty7) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Empty7) ProtoMessage() {}

func (x *Empty7) ProtoReflect() protoreflect.Message {
	mi := &file_schedule_week_service_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Empty7.ProtoReflect.Descriptor instead.
func (*Empty7) Descriptor() ([]byte, []int) {
	return file_schedule_week_service_proto_rawDescGZIP(), []int{0}
}

type ScheduleWeekPrimaryKey struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id string `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
}

func (x *ScheduleWeekPrimaryKey) Reset() {
	*x = ScheduleWeekPrimaryKey{}
	if protoimpl.UnsafeEnabled {
		mi := &file_schedule_week_service_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *ScheduleWeekPrimaryKey) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*ScheduleWeekPrimaryKey) ProtoMessage() {}

func (x *ScheduleWeekPrimaryKey) ProtoReflect() protoreflect.Message {
	mi := &file_schedule_week_service_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use ScheduleWeekPrimaryKey.ProtoReflect.Descriptor instead.
func (*ScheduleWeekPrimaryKey) Descriptor() ([]byte, []int) {
	return file_schedule_week_service_proto_rawDescGZIP(), []int{1}
}

func (x *ScheduleWeekPrimaryKey) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

type CreateScheduleWeek struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	GroupId          string `protobuf:"bytes,1,opt,name=group_id,json=groupId,proto3" json:"group_id,omitempty"`
	GroupType        string `protobuf:"bytes,2,opt,name=group_type,json=groupType,proto3" json:"group_type,omitempty"`
	NumberOfStudents string `protobuf:"bytes,3,opt,name=number_of_students,json=numberOfStudents,proto3" json:"number_of_students,omitempty"`
	StartTime        string `protobuf:"bytes,4,opt,name=start_time,json=startTime,proto3" json:"start_time,omitempty"`
	EndTime          string `protobuf:"bytes,5,opt,name=end_time,json=endTime,proto3" json:"end_time,omitempty"`
	BranchId         string `protobuf:"bytes,6,opt,name=branch_id,json=branchId,proto3" json:"branch_id,omitempty"`
	TeacherId        string `protobuf:"bytes,7,opt,name=teacher_id,json=teacherId,proto3" json:"teacher_id,omitempty"`
	SupportTeacherId string `protobuf:"bytes,8,opt,name=support_teacher_id,json=supportTeacherId,proto3" json:"support_teacher_id,omitempty"`
}

func (x *CreateScheduleWeek) Reset() {
	*x = CreateScheduleWeek{}
	if protoimpl.UnsafeEnabled {
		mi := &file_schedule_week_service_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *CreateScheduleWeek) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*CreateScheduleWeek) ProtoMessage() {}

func (x *CreateScheduleWeek) ProtoReflect() protoreflect.Message {
	mi := &file_schedule_week_service_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use CreateScheduleWeek.ProtoReflect.Descriptor instead.
func (*CreateScheduleWeek) Descriptor() ([]byte, []int) {
	return file_schedule_week_service_proto_rawDescGZIP(), []int{2}
}

func (x *CreateScheduleWeek) GetGroupId() string {
	if x != nil {
		return x.GroupId
	}
	return ""
}

func (x *CreateScheduleWeek) GetGroupType() string {
	if x != nil {
		return x.GroupType
	}
	return ""
}

func (x *CreateScheduleWeek) GetNumberOfStudents() string {
	if x != nil {
		return x.NumberOfStudents
	}
	return ""
}

func (x *CreateScheduleWeek) GetStartTime() string {
	if x != nil {
		return x.StartTime
	}
	return ""
}

func (x *CreateScheduleWeek) GetEndTime() string {
	if x != nil {
		return x.EndTime
	}
	return ""
}

func (x *CreateScheduleWeek) GetBranchId() string {
	if x != nil {
		return x.BranchId
	}
	return ""
}

func (x *CreateScheduleWeek) GetTeacherId() string {
	if x != nil {
		return x.TeacherId
	}
	return ""
}

func (x *CreateScheduleWeek) GetSupportTeacherId() string {
	if x != nil {
		return x.SupportTeacherId
	}
	return ""
}

type ScheduleWeek struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id               string `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	GroupId          string `protobuf:"bytes,2,opt,name=group_id,json=groupId,proto3" json:"group_id,omitempty"`
	GroupType        string `protobuf:"bytes,3,opt,name=group_type,json=groupType,proto3" json:"group_type,omitempty"`
	NumberOfStudents string `protobuf:"bytes,4,opt,name=number_of_students,json=numberOfStudents,proto3" json:"number_of_students,omitempty"`
	StartTime        string `protobuf:"bytes,5,opt,name=start_time,json=startTime,proto3" json:"start_time,omitempty"`
	EndTime          string `protobuf:"bytes,6,opt,name=end_time,json=endTime,proto3" json:"end_time,omitempty"`
	BranchId         string `protobuf:"bytes,7,opt,name=branch_id,json=branchId,proto3" json:"branch_id,omitempty"`
	TeacherId        string `protobuf:"bytes,8,opt,name=teacher_id,json=teacherId,proto3" json:"teacher_id,omitempty"`
	SupportTeacherId string `protobuf:"bytes,9,opt,name=support_teacher_id,json=supportTeacherId,proto3" json:"support_teacher_id,omitempty"`
	CreatedAt        string `protobuf:"bytes,10,opt,name=created_at,json=createdAt,proto3" json:"created_at,omitempty"`
	UpdatedAt        string `protobuf:"bytes,11,opt,name=updated_at,json=updatedAt,proto3" json:"updated_at,omitempty"`
}

func (x *ScheduleWeek) Reset() {
	*x = ScheduleWeek{}
	if protoimpl.UnsafeEnabled {
		mi := &file_schedule_week_service_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *ScheduleWeek) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*ScheduleWeek) ProtoMessage() {}

func (x *ScheduleWeek) ProtoReflect() protoreflect.Message {
	mi := &file_schedule_week_service_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use ScheduleWeek.ProtoReflect.Descriptor instead.
func (*ScheduleWeek) Descriptor() ([]byte, []int) {
	return file_schedule_week_service_proto_rawDescGZIP(), []int{3}
}

func (x *ScheduleWeek) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

func (x *ScheduleWeek) GetGroupId() string {
	if x != nil {
		return x.GroupId
	}
	return ""
}

func (x *ScheduleWeek) GetGroupType() string {
	if x != nil {
		return x.GroupType
	}
	return ""
}

func (x *ScheduleWeek) GetNumberOfStudents() string {
	if x != nil {
		return x.NumberOfStudents
	}
	return ""
}

func (x *ScheduleWeek) GetStartTime() string {
	if x != nil {
		return x.StartTime
	}
	return ""
}

func (x *ScheduleWeek) GetEndTime() string {
	if x != nil {
		return x.EndTime
	}
	return ""
}

func (x *ScheduleWeek) GetBranchId() string {
	if x != nil {
		return x.BranchId
	}
	return ""
}

func (x *ScheduleWeek) GetTeacherId() string {
	if x != nil {
		return x.TeacherId
	}
	return ""
}

func (x *ScheduleWeek) GetSupportTeacherId() string {
	if x != nil {
		return x.SupportTeacherId
	}
	return ""
}

func (x *ScheduleWeek) GetCreatedAt() string {
	if x != nil {
		return x.CreatedAt
	}
	return ""
}

func (x *ScheduleWeek) GetUpdatedAt() string {
	if x != nil {
		return x.UpdatedAt
	}
	return ""
}

type UpdateScheduleWeek struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id               string `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	GroupId          string `protobuf:"bytes,2,opt,name=group_id,json=groupId,proto3" json:"group_id,omitempty"`
	GroupType        string `protobuf:"bytes,3,opt,name=group_type,json=groupType,proto3" json:"group_type,omitempty"`
	NumberOfStudents string `protobuf:"bytes,4,opt,name=number_of_students,json=numberOfStudents,proto3" json:"number_of_students,omitempty"`
	StartTime        string `protobuf:"bytes,5,opt,name=start_time,json=startTime,proto3" json:"start_time,omitempty"`
	EndTime          string `protobuf:"bytes,6,opt,name=end_time,json=endTime,proto3" json:"end_time,omitempty"`
	BranchId         string `protobuf:"bytes,7,opt,name=branch_id,json=branchId,proto3" json:"branch_id,omitempty"`
	TeacherId        string `protobuf:"bytes,8,opt,name=teacher_id,json=teacherId,proto3" json:"teacher_id,omitempty"`
	SupportTeacherId string `protobuf:"bytes,9,opt,name=support_teacher_id,json=supportTeacherId,proto3" json:"support_teacher_id,omitempty"`
}

func (x *UpdateScheduleWeek) Reset() {
	*x = UpdateScheduleWeek{}
	if protoimpl.UnsafeEnabled {
		mi := &file_schedule_week_service_proto_msgTypes[4]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *UpdateScheduleWeek) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*UpdateScheduleWeek) ProtoMessage() {}

func (x *UpdateScheduleWeek) ProtoReflect() protoreflect.Message {
	mi := &file_schedule_week_service_proto_msgTypes[4]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use UpdateScheduleWeek.ProtoReflect.Descriptor instead.
func (*UpdateScheduleWeek) Descriptor() ([]byte, []int) {
	return file_schedule_week_service_proto_rawDescGZIP(), []int{4}
}

func (x *UpdateScheduleWeek) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

func (x *UpdateScheduleWeek) GetGroupId() string {
	if x != nil {
		return x.GroupId
	}
	return ""
}

func (x *UpdateScheduleWeek) GetGroupType() string {
	if x != nil {
		return x.GroupType
	}
	return ""
}

func (x *UpdateScheduleWeek) GetNumberOfStudents() string {
	if x != nil {
		return x.NumberOfStudents
	}
	return ""
}

func (x *UpdateScheduleWeek) GetStartTime() string {
	if x != nil {
		return x.StartTime
	}
	return ""
}

func (x *UpdateScheduleWeek) GetEndTime() string {
	if x != nil {
		return x.EndTime
	}
	return ""
}

func (x *UpdateScheduleWeek) GetBranchId() string {
	if x != nil {
		return x.BranchId
	}
	return ""
}

func (x *UpdateScheduleWeek) GetTeacherId() string {
	if x != nil {
		return x.TeacherId
	}
	return ""
}

func (x *UpdateScheduleWeek) GetSupportTeacherId() string {
	if x != nil {
		return x.SupportTeacherId
	}
	return ""
}

type GetListScheduleWeekRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Offset int64  `protobuf:"varint,1,opt,name=offset,proto3" json:"offset,omitempty"`
	Limit  int64  `protobuf:"varint,2,opt,name=limit,proto3" json:"limit,omitempty"`
	Search string `protobuf:"bytes,3,opt,name=search,proto3" json:"search,omitempty"`
}

func (x *GetListScheduleWeekRequest) Reset() {
	*x = GetListScheduleWeekRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_schedule_week_service_proto_msgTypes[5]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetListScheduleWeekRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetListScheduleWeekRequest) ProtoMessage() {}

func (x *GetListScheduleWeekRequest) ProtoReflect() protoreflect.Message {
	mi := &file_schedule_week_service_proto_msgTypes[5]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetListScheduleWeekRequest.ProtoReflect.Descriptor instead.
func (*GetListScheduleWeekRequest) Descriptor() ([]byte, []int) {
	return file_schedule_week_service_proto_rawDescGZIP(), []int{5}
}

func (x *GetListScheduleWeekRequest) GetOffset() int64 {
	if x != nil {
		return x.Offset
	}
	return 0
}

func (x *GetListScheduleWeekRequest) GetLimit() int64 {
	if x != nil {
		return x.Limit
	}
	return 0
}

func (x *GetListScheduleWeekRequest) GetSearch() string {
	if x != nil {
		return x.Search
	}
	return ""
}

type GetListScheduleWeekResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Count         int64           `protobuf:"varint,1,opt,name=Count,proto3" json:"Count,omitempty"`
	ScheduleWeeks []*ScheduleWeek `protobuf:"bytes,2,rep,name=ScheduleWeeks,proto3" json:"ScheduleWeeks,omitempty"`
}

func (x *GetListScheduleWeekResponse) Reset() {
	*x = GetListScheduleWeekResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_schedule_week_service_proto_msgTypes[6]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetListScheduleWeekResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetListScheduleWeekResponse) ProtoMessage() {}

func (x *GetListScheduleWeekResponse) ProtoReflect() protoreflect.Message {
	mi := &file_schedule_week_service_proto_msgTypes[6]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetListScheduleWeekResponse.ProtoReflect.Descriptor instead.
func (*GetListScheduleWeekResponse) Descriptor() ([]byte, []int) {
	return file_schedule_week_service_proto_rawDescGZIP(), []int{6}
}

func (x *GetListScheduleWeekResponse) GetCount() int64 {
	if x != nil {
		return x.Count
	}
	return 0
}

func (x *GetListScheduleWeekResponse) GetScheduleWeeks() []*ScheduleWeek {
	if x != nil {
		return x.ScheduleWeeks
	}
	return nil
}

var File_schedule_week_service_proto protoreflect.FileDescriptor

var file_schedule_week_service_proto_rawDesc = []byte{
	0x0a, 0x1b, 0x73, 0x63, 0x68, 0x65, 0x64, 0x75, 0x6c, 0x65, 0x5f, 0x77, 0x65, 0x65, 0x6b, 0x5f,
	0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x0d, 0x61,
	0x64, 0x6d, 0x69, 0x6e, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x22, 0x08, 0x0a, 0x06,
	0x45, 0x6d, 0x70, 0x74, 0x79, 0x37, 0x22, 0x28, 0x0a, 0x16, 0x53, 0x63, 0x68, 0x65, 0x64, 0x75,
	0x6c, 0x65, 0x57, 0x65, 0x65, 0x6b, 0x50, 0x72, 0x69, 0x6d, 0x61, 0x72, 0x79, 0x4b, 0x65, 0x79,
	0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x02, 0x69, 0x64,
	0x22, 0xa0, 0x02, 0x0a, 0x12, 0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x53, 0x63, 0x68, 0x65, 0x64,
	0x75, 0x6c, 0x65, 0x57, 0x65, 0x65, 0x6b, 0x12, 0x19, 0x0a, 0x08, 0x67, 0x72, 0x6f, 0x75, 0x70,
	0x5f, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x07, 0x67, 0x72, 0x6f, 0x75, 0x70,
	0x49, 0x64, 0x12, 0x1d, 0x0a, 0x0a, 0x67, 0x72, 0x6f, 0x75, 0x70, 0x5f, 0x74, 0x79, 0x70, 0x65,
	0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x67, 0x72, 0x6f, 0x75, 0x70, 0x54, 0x79, 0x70,
	0x65, 0x12, 0x2c, 0x0a, 0x12, 0x6e, 0x75, 0x6d, 0x62, 0x65, 0x72, 0x5f, 0x6f, 0x66, 0x5f, 0x73,
	0x74, 0x75, 0x64, 0x65, 0x6e, 0x74, 0x73, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x10, 0x6e,
	0x75, 0x6d, 0x62, 0x65, 0x72, 0x4f, 0x66, 0x53, 0x74, 0x75, 0x64, 0x65, 0x6e, 0x74, 0x73, 0x12,
	0x1d, 0x0a, 0x0a, 0x73, 0x74, 0x61, 0x72, 0x74, 0x5f, 0x74, 0x69, 0x6d, 0x65, 0x18, 0x04, 0x20,
	0x01, 0x28, 0x09, 0x52, 0x09, 0x73, 0x74, 0x61, 0x72, 0x74, 0x54, 0x69, 0x6d, 0x65, 0x12, 0x19,
	0x0a, 0x08, 0x65, 0x6e, 0x64, 0x5f, 0x74, 0x69, 0x6d, 0x65, 0x18, 0x05, 0x20, 0x01, 0x28, 0x09,
	0x52, 0x07, 0x65, 0x6e, 0x64, 0x54, 0x69, 0x6d, 0x65, 0x12, 0x1b, 0x0a, 0x09, 0x62, 0x72, 0x61,
	0x6e, 0x63, 0x68, 0x5f, 0x69, 0x64, 0x18, 0x06, 0x20, 0x01, 0x28, 0x09, 0x52, 0x08, 0x62, 0x72,
	0x61, 0x6e, 0x63, 0x68, 0x49, 0x64, 0x12, 0x1d, 0x0a, 0x0a, 0x74, 0x65, 0x61, 0x63, 0x68, 0x65,
	0x72, 0x5f, 0x69, 0x64, 0x18, 0x07, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x74, 0x65, 0x61, 0x63,
	0x68, 0x65, 0x72, 0x49, 0x64, 0x12, 0x2c, 0x0a, 0x12, 0x73, 0x75, 0x70, 0x70, 0x6f, 0x72, 0x74,
	0x5f, 0x74, 0x65, 0x61, 0x63, 0x68, 0x65, 0x72, 0x5f, 0x69, 0x64, 0x18, 0x08, 0x20, 0x01, 0x28,
	0x09, 0x52, 0x10, 0x73, 0x75, 0x70, 0x70, 0x6f, 0x72, 0x74, 0x54, 0x65, 0x61, 0x63, 0x68, 0x65,
	0x72, 0x49, 0x64, 0x22, 0xe8, 0x02, 0x0a, 0x0c, 0x53, 0x63, 0x68, 0x65, 0x64, 0x75, 0x6c, 0x65,
	0x57, 0x65, 0x65, 0x6b, 0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09,
	0x52, 0x02, 0x69, 0x64, 0x12, 0x19, 0x0a, 0x08, 0x67, 0x72, 0x6f, 0x75, 0x70, 0x5f, 0x69, 0x64,
	0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x07, 0x67, 0x72, 0x6f, 0x75, 0x70, 0x49, 0x64, 0x12,
	0x1d, 0x0a, 0x0a, 0x67, 0x72, 0x6f, 0x75, 0x70, 0x5f, 0x74, 0x79, 0x70, 0x65, 0x18, 0x03, 0x20,
	0x01, 0x28, 0x09, 0x52, 0x09, 0x67, 0x72, 0x6f, 0x75, 0x70, 0x54, 0x79, 0x70, 0x65, 0x12, 0x2c,
	0x0a, 0x12, 0x6e, 0x75, 0x6d, 0x62, 0x65, 0x72, 0x5f, 0x6f, 0x66, 0x5f, 0x73, 0x74, 0x75, 0x64,
	0x65, 0x6e, 0x74, 0x73, 0x18, 0x04, 0x20, 0x01, 0x28, 0x09, 0x52, 0x10, 0x6e, 0x75, 0x6d, 0x62,
	0x65, 0x72, 0x4f, 0x66, 0x53, 0x74, 0x75, 0x64, 0x65, 0x6e, 0x74, 0x73, 0x12, 0x1d, 0x0a, 0x0a,
	0x73, 0x74, 0x61, 0x72, 0x74, 0x5f, 0x74, 0x69, 0x6d, 0x65, 0x18, 0x05, 0x20, 0x01, 0x28, 0x09,
	0x52, 0x09, 0x73, 0x74, 0x61, 0x72, 0x74, 0x54, 0x69, 0x6d, 0x65, 0x12, 0x19, 0x0a, 0x08, 0x65,
	0x6e, 0x64, 0x5f, 0x74, 0x69, 0x6d, 0x65, 0x18, 0x06, 0x20, 0x01, 0x28, 0x09, 0x52, 0x07, 0x65,
	0x6e, 0x64, 0x54, 0x69, 0x6d, 0x65, 0x12, 0x1b, 0x0a, 0x09, 0x62, 0x72, 0x61, 0x6e, 0x63, 0x68,
	0x5f, 0x69, 0x64, 0x18, 0x07, 0x20, 0x01, 0x28, 0x09, 0x52, 0x08, 0x62, 0x72, 0x61, 0x6e, 0x63,
	0x68, 0x49, 0x64, 0x12, 0x1d, 0x0a, 0x0a, 0x74, 0x65, 0x61, 0x63, 0x68, 0x65, 0x72, 0x5f, 0x69,
	0x64, 0x18, 0x08, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x74, 0x65, 0x61, 0x63, 0x68, 0x65, 0x72,
	0x49, 0x64, 0x12, 0x2c, 0x0a, 0x12, 0x73, 0x75, 0x70, 0x70, 0x6f, 0x72, 0x74, 0x5f, 0x74, 0x65,
	0x61, 0x63, 0x68, 0x65, 0x72, 0x5f, 0x69, 0x64, 0x18, 0x09, 0x20, 0x01, 0x28, 0x09, 0x52, 0x10,
	0x73, 0x75, 0x70, 0x70, 0x6f, 0x72, 0x74, 0x54, 0x65, 0x61, 0x63, 0x68, 0x65, 0x72, 0x49, 0x64,
	0x12, 0x1d, 0x0a, 0x0a, 0x63, 0x72, 0x65, 0x61, 0x74, 0x65, 0x64, 0x5f, 0x61, 0x74, 0x18, 0x0a,
	0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x63, 0x72, 0x65, 0x61, 0x74, 0x65, 0x64, 0x41, 0x74, 0x12,
	0x1d, 0x0a, 0x0a, 0x75, 0x70, 0x64, 0x61, 0x74, 0x65, 0x64, 0x5f, 0x61, 0x74, 0x18, 0x0b, 0x20,
	0x01, 0x28, 0x09, 0x52, 0x09, 0x75, 0x70, 0x64, 0x61, 0x74, 0x65, 0x64, 0x41, 0x74, 0x22, 0xb0,
	0x02, 0x0a, 0x12, 0x55, 0x70, 0x64, 0x61, 0x74, 0x65, 0x53, 0x63, 0x68, 0x65, 0x64, 0x75, 0x6c,
	0x65, 0x57, 0x65, 0x65, 0x6b, 0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28,
	0x09, 0x52, 0x02, 0x69, 0x64, 0x12, 0x19, 0x0a, 0x08, 0x67, 0x72, 0x6f, 0x75, 0x70, 0x5f, 0x69,
	0x64, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x07, 0x67, 0x72, 0x6f, 0x75, 0x70, 0x49, 0x64,
	0x12, 0x1d, 0x0a, 0x0a, 0x67, 0x72, 0x6f, 0x75, 0x70, 0x5f, 0x74, 0x79, 0x70, 0x65, 0x18, 0x03,
	0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x67, 0x72, 0x6f, 0x75, 0x70, 0x54, 0x79, 0x70, 0x65, 0x12,
	0x2c, 0x0a, 0x12, 0x6e, 0x75, 0x6d, 0x62, 0x65, 0x72, 0x5f, 0x6f, 0x66, 0x5f, 0x73, 0x74, 0x75,
	0x64, 0x65, 0x6e, 0x74, 0x73, 0x18, 0x04, 0x20, 0x01, 0x28, 0x09, 0x52, 0x10, 0x6e, 0x75, 0x6d,
	0x62, 0x65, 0x72, 0x4f, 0x66, 0x53, 0x74, 0x75, 0x64, 0x65, 0x6e, 0x74, 0x73, 0x12, 0x1d, 0x0a,
	0x0a, 0x73, 0x74, 0x61, 0x72, 0x74, 0x5f, 0x74, 0x69, 0x6d, 0x65, 0x18, 0x05, 0x20, 0x01, 0x28,
	0x09, 0x52, 0x09, 0x73, 0x74, 0x61, 0x72, 0x74, 0x54, 0x69, 0x6d, 0x65, 0x12, 0x19, 0x0a, 0x08,
	0x65, 0x6e, 0x64, 0x5f, 0x74, 0x69, 0x6d, 0x65, 0x18, 0x06, 0x20, 0x01, 0x28, 0x09, 0x52, 0x07,
	0x65, 0x6e, 0x64, 0x54, 0x69, 0x6d, 0x65, 0x12, 0x1b, 0x0a, 0x09, 0x62, 0x72, 0x61, 0x6e, 0x63,
	0x68, 0x5f, 0x69, 0x64, 0x18, 0x07, 0x20, 0x01, 0x28, 0x09, 0x52, 0x08, 0x62, 0x72, 0x61, 0x6e,
	0x63, 0x68, 0x49, 0x64, 0x12, 0x1d, 0x0a, 0x0a, 0x74, 0x65, 0x61, 0x63, 0x68, 0x65, 0x72, 0x5f,
	0x69, 0x64, 0x18, 0x08, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x74, 0x65, 0x61, 0x63, 0x68, 0x65,
	0x72, 0x49, 0x64, 0x12, 0x2c, 0x0a, 0x12, 0x73, 0x75, 0x70, 0x70, 0x6f, 0x72, 0x74, 0x5f, 0x74,
	0x65, 0x61, 0x63, 0x68, 0x65, 0x72, 0x5f, 0x69, 0x64, 0x18, 0x09, 0x20, 0x01, 0x28, 0x09, 0x52,
	0x10, 0x73, 0x75, 0x70, 0x70, 0x6f, 0x72, 0x74, 0x54, 0x65, 0x61, 0x63, 0x68, 0x65, 0x72, 0x49,
	0x64, 0x22, 0x62, 0x0a, 0x1a, 0x47, 0x65, 0x74, 0x4c, 0x69, 0x73, 0x74, 0x53, 0x63, 0x68, 0x65,
	0x64, 0x75, 0x6c, 0x65, 0x57, 0x65, 0x65, 0x6b, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12,
	0x16, 0x0a, 0x06, 0x6f, 0x66, 0x66, 0x73, 0x65, 0x74, 0x18, 0x01, 0x20, 0x01, 0x28, 0x03, 0x52,
	0x06, 0x6f, 0x66, 0x66, 0x73, 0x65, 0x74, 0x12, 0x14, 0x0a, 0x05, 0x6c, 0x69, 0x6d, 0x69, 0x74,
	0x18, 0x02, 0x20, 0x01, 0x28, 0x03, 0x52, 0x05, 0x6c, 0x69, 0x6d, 0x69, 0x74, 0x12, 0x16, 0x0a,
	0x06, 0x73, 0x65, 0x61, 0x72, 0x63, 0x68, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x06, 0x73,
	0x65, 0x61, 0x72, 0x63, 0x68, 0x22, 0x76, 0x0a, 0x1b, 0x47, 0x65, 0x74, 0x4c, 0x69, 0x73, 0x74,
	0x53, 0x63, 0x68, 0x65, 0x64, 0x75, 0x6c, 0x65, 0x57, 0x65, 0x65, 0x6b, 0x52, 0x65, 0x73, 0x70,
	0x6f, 0x6e, 0x73, 0x65, 0x12, 0x14, 0x0a, 0x05, 0x43, 0x6f, 0x75, 0x6e, 0x74, 0x18, 0x01, 0x20,
	0x01, 0x28, 0x03, 0x52, 0x05, 0x43, 0x6f, 0x75, 0x6e, 0x74, 0x12, 0x41, 0x0a, 0x0d, 0x53, 0x63,
	0x68, 0x65, 0x64, 0x75, 0x6c, 0x65, 0x57, 0x65, 0x65, 0x6b, 0x73, 0x18, 0x02, 0x20, 0x03, 0x28,
	0x0b, 0x32, 0x1b, 0x2e, 0x61, 0x64, 0x6d, 0x69, 0x6e, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63,
	0x65, 0x2e, 0x53, 0x63, 0x68, 0x65, 0x64, 0x75, 0x6c, 0x65, 0x57, 0x65, 0x65, 0x6b, 0x52, 0x0d,
	0x53, 0x63, 0x68, 0x65, 0x64, 0x75, 0x6c, 0x65, 0x57, 0x65, 0x65, 0x6b, 0x73, 0x32, 0xac, 0x03,
	0x0a, 0x13, 0x53, 0x63, 0x68, 0x65, 0x64, 0x75, 0x6c, 0x65, 0x57, 0x65, 0x65, 0x6b, 0x53, 0x65,
	0x72, 0x76, 0x69, 0x63, 0x65, 0x12, 0x4a, 0x0a, 0x06, 0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x12,
	0x21, 0x2e, 0x61, 0x64, 0x6d, 0x69, 0x6e, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e,
	0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x53, 0x63, 0x68, 0x65, 0x64, 0x75, 0x6c, 0x65, 0x57, 0x65,
	0x65, 0x6b, 0x1a, 0x1b, 0x2e, 0x61, 0x64, 0x6d, 0x69, 0x6e, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69,
	0x63, 0x65, 0x2e, 0x53, 0x63, 0x68, 0x65, 0x64, 0x75, 0x6c, 0x65, 0x57, 0x65, 0x65, 0x6b, 0x22,
	0x00, 0x12, 0x4f, 0x0a, 0x07, 0x47, 0x65, 0x74, 0x42, 0x79, 0x49, 0x44, 0x12, 0x25, 0x2e, 0x61,
	0x64, 0x6d, 0x69, 0x6e, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x53, 0x63, 0x68,
	0x65, 0x64, 0x75, 0x6c, 0x65, 0x57, 0x65, 0x65, 0x6b, 0x50, 0x72, 0x69, 0x6d, 0x61, 0x72, 0x79,
	0x4b, 0x65, 0x79, 0x1a, 0x1b, 0x2e, 0x61, 0x64, 0x6d, 0x69, 0x6e, 0x5f, 0x73, 0x65, 0x72, 0x76,
	0x69, 0x63, 0x65, 0x2e, 0x53, 0x63, 0x68, 0x65, 0x64, 0x75, 0x6c, 0x65, 0x57, 0x65, 0x65, 0x6b,
	0x22, 0x00, 0x12, 0x62, 0x0a, 0x07, 0x47, 0x65, 0x74, 0x4c, 0x69, 0x73, 0x74, 0x12, 0x29, 0x2e,
	0x61, 0x64, 0x6d, 0x69, 0x6e, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x47, 0x65,
	0x74, 0x4c, 0x69, 0x73, 0x74, 0x53, 0x63, 0x68, 0x65, 0x64, 0x75, 0x6c, 0x65, 0x57, 0x65, 0x65,
	0x6b, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x2a, 0x2e, 0x61, 0x64, 0x6d, 0x69, 0x6e,
	0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x47, 0x65, 0x74, 0x4c, 0x69, 0x73, 0x74,
	0x53, 0x63, 0x68, 0x65, 0x64, 0x75, 0x6c, 0x65, 0x57, 0x65, 0x65, 0x6b, 0x52, 0x65, 0x73, 0x70,
	0x6f, 0x6e, 0x73, 0x65, 0x22, 0x00, 0x12, 0x4a, 0x0a, 0x06, 0x55, 0x70, 0x64, 0x61, 0x74, 0x65,
	0x12, 0x21, 0x2e, 0x61, 0x64, 0x6d, 0x69, 0x6e, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65,
	0x2e, 0x55, 0x70, 0x64, 0x61, 0x74, 0x65, 0x53, 0x63, 0x68, 0x65, 0x64, 0x75, 0x6c, 0x65, 0x57,
	0x65, 0x65, 0x6b, 0x1a, 0x1b, 0x2e, 0x61, 0x64, 0x6d, 0x69, 0x6e, 0x5f, 0x73, 0x65, 0x72, 0x76,
	0x69, 0x63, 0x65, 0x2e, 0x53, 0x63, 0x68, 0x65, 0x64, 0x75, 0x6c, 0x65, 0x57, 0x65, 0x65, 0x6b,
	0x22, 0x00, 0x12, 0x48, 0x0a, 0x06, 0x44, 0x65, 0x6c, 0x65, 0x74, 0x65, 0x12, 0x25, 0x2e, 0x61,
	0x64, 0x6d, 0x69, 0x6e, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x53, 0x63, 0x68,
	0x65, 0x64, 0x75, 0x6c, 0x65, 0x57, 0x65, 0x65, 0x6b, 0x50, 0x72, 0x69, 0x6d, 0x61, 0x72, 0x79,
	0x4b, 0x65, 0x79, 0x1a, 0x15, 0x2e, 0x61, 0x64, 0x6d, 0x69, 0x6e, 0x5f, 0x73, 0x65, 0x72, 0x76,
	0x69, 0x63, 0x65, 0x2e, 0x45, 0x6d, 0x70, 0x74, 0x79, 0x37, 0x22, 0x00, 0x42, 0x18, 0x5a, 0x16,
	0x67, 0x65, 0x6e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2f, 0x61, 0x64, 0x6d, 0x69, 0x6e, 0x5f, 0x73,
	0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_schedule_week_service_proto_rawDescOnce sync.Once
	file_schedule_week_service_proto_rawDescData = file_schedule_week_service_proto_rawDesc
)

func file_schedule_week_service_proto_rawDescGZIP() []byte {
	file_schedule_week_service_proto_rawDescOnce.Do(func() {
		file_schedule_week_service_proto_rawDescData = protoimpl.X.CompressGZIP(file_schedule_week_service_proto_rawDescData)
	})
	return file_schedule_week_service_proto_rawDescData
}

var file_schedule_week_service_proto_msgTypes = make([]protoimpl.MessageInfo, 7)
var file_schedule_week_service_proto_goTypes = []interface{}{
	(*Empty7)(nil),                      // 0: admin_service.Empty7
	(*ScheduleWeekPrimaryKey)(nil),      // 1: admin_service.ScheduleWeekPrimaryKey
	(*CreateScheduleWeek)(nil),          // 2: admin_service.CreateScheduleWeek
	(*ScheduleWeek)(nil),                // 3: admin_service.ScheduleWeek
	(*UpdateScheduleWeek)(nil),          // 4: admin_service.UpdateScheduleWeek
	(*GetListScheduleWeekRequest)(nil),  // 5: admin_service.GetListScheduleWeekRequest
	(*GetListScheduleWeekResponse)(nil), // 6: admin_service.GetListScheduleWeekResponse
}
var file_schedule_week_service_proto_depIdxs = []int32{
	3, // 0: admin_service.GetListScheduleWeekResponse.ScheduleWeeks:type_name -> admin_service.ScheduleWeek
	2, // 1: admin_service.ScheduleWeekService.Create:input_type -> admin_service.CreateScheduleWeek
	1, // 2: admin_service.ScheduleWeekService.GetByID:input_type -> admin_service.ScheduleWeekPrimaryKey
	5, // 3: admin_service.ScheduleWeekService.GetList:input_type -> admin_service.GetListScheduleWeekRequest
	4, // 4: admin_service.ScheduleWeekService.Update:input_type -> admin_service.UpdateScheduleWeek
	1, // 5: admin_service.ScheduleWeekService.Delete:input_type -> admin_service.ScheduleWeekPrimaryKey
	3, // 6: admin_service.ScheduleWeekService.Create:output_type -> admin_service.ScheduleWeek
	3, // 7: admin_service.ScheduleWeekService.GetByID:output_type -> admin_service.ScheduleWeek
	6, // 8: admin_service.ScheduleWeekService.GetList:output_type -> admin_service.GetListScheduleWeekResponse
	3, // 9: admin_service.ScheduleWeekService.Update:output_type -> admin_service.ScheduleWeek
	0, // 10: admin_service.ScheduleWeekService.Delete:output_type -> admin_service.Empty7
	6, // [6:11] is the sub-list for method output_type
	1, // [1:6] is the sub-list for method input_type
	1, // [1:1] is the sub-list for extension type_name
	1, // [1:1] is the sub-list for extension extendee
	0, // [0:1] is the sub-list for field type_name
}

func init() { file_schedule_week_service_proto_init() }
func file_schedule_week_service_proto_init() {
	if File_schedule_week_service_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_schedule_week_service_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Empty7); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_schedule_week_service_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*ScheduleWeekPrimaryKey); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_schedule_week_service_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*CreateScheduleWeek); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_schedule_week_service_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*ScheduleWeek); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_schedule_week_service_proto_msgTypes[4].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*UpdateScheduleWeek); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_schedule_week_service_proto_msgTypes[5].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GetListScheduleWeekRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_schedule_week_service_proto_msgTypes[6].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GetListScheduleWeekResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_schedule_week_service_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   7,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_schedule_week_service_proto_goTypes,
		DependencyIndexes: file_schedule_week_service_proto_depIdxs,
		MessageInfos:      file_schedule_week_service_proto_msgTypes,
	}.Build()
	File_schedule_week_service_proto = out.File
	file_schedule_week_service_proto_rawDesc = nil
	file_schedule_week_service_proto_goTypes = nil
	file_schedule_week_service_proto_depIdxs = nil
}
